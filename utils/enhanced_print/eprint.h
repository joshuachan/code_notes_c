/* ***************************************************************
 * File name: eprint.h
 * Function:  增强打印功能
 * Description: 在打印信息中加入: 当前函数名和行号, 便于调试定位
 *              打印普通交互信息使用printf(), 打印调试信息用DBG(),
 *              打印一般出错信息使用PRINTF(), 以替代printf(),
 *              打印系统调用出错息使用PERROR(), 以替代perror()
 * Author & Date: Joshua Chan, 2012/01/10
 * ***************************************************************/

#ifndef _EPRINT_H
#define _EPRINT_H

/* 打印调试信息, 可以在编译时加-DDEBUG参数开启 */
#ifndef DEBUG
#define DBG(fmt, args...) do {} while (0)
#else
#define DBG(fmt, args...) printf("[%s:%d] "fmt, __func__, __LINE__, ##args)
#endif

/* 打印一般出错信息 */
#define PRINTF(fmt, args...) printf("[%s:%d] "fmt, __func__, __LINE__, ##args)

/* 打印系统调用出错信息 */
#define PERROR(fmt) fprintf(stderr, "[%s:%d] "fmt": %m\n", __func__, __LINE__)

#endif