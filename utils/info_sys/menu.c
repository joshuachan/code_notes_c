/* *******************************************************
 * Name: menu.c
 * Proj: 简单信息管理系统
 * Desc: 包括数据操作有: 增加/遍历/排序/查询及删除匹配数据
 *        /数据文件的导入和导出
 * Author & Date: Joshua Chan, 2011/11/08
 * ******************************************************/
#include <stdio.h>
#include "main.h"

/* 显示界面        */
void disp_menu(void)
{
    printf("\n*** Information Management System ***\n");
    printf("=====================================\n");
    printf("  [1] Add Student Record\n");
    printf("  [2] Student Information Query\n");
    printf("  [3] Sort Student by ID\n");
    printf("  [4] Search Student by ID\n");
    printf("  [5] Remove Student by ID\n");
    printf("  [6] Save Manually\n\n");
    printf("  [0] Exit\n");
}

/* 处理界面菜单选择        */
void select_menu(LIST l)
{
    char *tmp;
    while (true) {
        printf("\nPlease input choice: ");
        tmp = str_trim(get_str());
        if (strncmp(tmp, "1", 1) == 0) {
            add_student(l);
            break;
        } else if (strncmp(tmp, "2", 1) == 0) {
            query_student(l);
            break;
        } else if (strncmp(tmp, "3", 1) == 0) {
            sort_student(l);
            break;
        } else if (strncmp(tmp, "4", 1) == 0) {
            search_student(l);
            break;
        } else if (strncmp(tmp, "5", 1) == 0) {
            remove_student(l);
            break;
        } else if (strncmp(tmp, "6", 1) == 0) {
            save_manual(l);
            break;
        } else if (strncmp(tmp, "0", 1) == 0) {
            return;
        } else {
            printf("Invalid selection.\n");
        }
    }
}