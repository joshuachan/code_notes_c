/* *******************************************************
 * Name: test.c
 * Proj: 简单信息管理系统
 * Desc: 包括数据操作有: 增加/遍历/排序/查询及删除匹配数据
 *        /数据文件的导入和导出
 * Author & Date: Joshua Chan, 2011/11/08
 * ******************************************************/
#include <stdio.h>
#include <string.h>
#include "main.h"

/* 测试程序        */
int main(void)
{
    /* 数据结构初始化        */
    LIST l = list_init(sizeof(struct student));

    /* 从数据文件导入记录    */
    import_data(l, PATH);

    /* 加载界面选单          */
    disp_menu();
    select_menu(l);

    /* 将记录存入文件        */
    export_data(l, PATH);
    list_release(l);
    return 0;
}