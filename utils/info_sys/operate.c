/* *******************************************************
 * Name: operate.c
 * Proj: 简单信息管理系统
 * Desc: 包括数据操作有: 增加/遍历/排序/查询及删除匹配数据
 *        /数据文件的导入和导出
 * Author & Date: Joshua Chan, 2011/11/08
 * ******************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"

/* 打印记录头信息        */
static void stud_title(void)
{
    printf("%-6s%-12s%-10s%-12s\n", "ID", "Name", "Gender", "Department");
    printf("----------------------------------------\n");
}

/* 打印一条记录信息      */
static void stud_print(void *s)
{
    struct student *stu = s;
    printf("%-6d%-12s%-10s%-12s\n", stu->id, stu->name,
            stu->gender, stu->depart);
}

/* 根据给定数据生成结构  */
static struct student put_data(int id, char *name, char *gender, char *depart)
{
    struct student stu;

    stu.id = id;
    strcpy(stu.name, name);
    strcpy(stu.gender, gender);
    strcpy(stu.depart, depart);

    return stu;
}

/* ID比较, 作为回调函数使用        */
static int comp_id(void *data1, void *data2)
{
    struct student *a = data1;
    struct student *b = data2;
    if (a->id == b->id)
        return 0;
    return (a->id > b->id) ? 1 : -1;
}

/* 增加记录        */
void add_student(LIST l)
{
    struct student stu;
    char *str;
    char **tmp;
    int id;
    char *name, *gender, *depart;

    while (true) {
        printf("\nPlease input information or just press enter to return.\n");
        printf("Format: ID, Name, Gender, Department\n");
        str = str_trim(get_str());
        tmp = &str;
        /* 输入字符串为空则返回        */
        if (strlen(str) == 0) {
            disp_menu();
            select_menu(l);
            break;
        }
        /* 字符串分割        */
        if (split_num(str, ",") == 4) {
            id = atoi(strsep(tmp, ","));
            name = str_trim(strsep(tmp, ","));
            gender = str_trim(strsep(tmp, ","));
            depart = str_trim(strsep(tmp, ","));

            stu = put_data(id, name, gender, depart);
            list_addlast(l, &stu);
            printf("New record was added, press enter to continue... ");
        } else {
            printf("Wrong format, press enter to continue... ");
        }
        get_str();
    }
}

/* 遍历数据, 并执行回调函数中的打印操作        */
void query_student(LIST l)
{
    putchar('\n');
    stud_title();
    if (!(list_travel(l, stud_print))) {
        printf("Total: %d %s\n", l->count,
                ((l->count > 1) ? "records." : "record."));
        printf("\nThere is no record.");
    } else {
        printf("Total: %d %s\n", l->count,
                ((l->count > 1) ? "records." : "record."));
    }
    printf("\nPress enter to continue... ");
    get_str();
    disp_menu();
    select_menu(l);
}

/* 按回调函数中的比较条件, 对数据进行排序        */
void sort_student(LIST l)
{
    if (list_sort(l, comp_id, true) == false)
        printf("There is no record.\n");
    else
        printf("All records were sorted.\n");
    printf("Press enter to continue... ");
    get_str();
    disp_menu();
    select_menu(l);
}

/* 查找匹配条件的记录        */
void search_student(LIST l)
{
    struct student stu;
    struct student *tmp;
    char *str;
    int id;
    char *name, *gender, *depart;

    printf("Please input ID number: ");
    str = str_trim(get_str());
    id = atoi(str);
    name = "";
    gender = "";
    depart = "";

    stu = put_data(id, name, gender, depart);
    if ((tmp = list_getmatch(l, comp_id, &stu)) != NULL) {
        putchar('\n');
        stud_title();
        stud_print(tmp);
        printf("\nRecord was found, press enter to continue... ");
    } else {
        printf("Record not found, press enter to continue... ");
    }
    get_str();
    disp_menu();
    select_menu(l);
}

/* 查找匹配条件的记录, 并提示删除该记录        */
void remove_student(LIST l)
{
    struct student stu;
    struct student *tmp;
    char *str;
    int id;
    char *name, *gender, *depart;

    printf("Please input ID number: ");
    str = str_trim(get_str());
    id = atoi(str);
    name = "";
    gender = "";
    depart = "";

    stu = put_data(id, name, gender, depart);
    if ((tmp = list_getmatch(l, comp_id, &stu)) != NULL) {
        putchar('\n');
        stud_title();
        stud_print(tmp);
        printf("\nRecord was found, REMOVE it please input y,\n"
                "or just press enter to cancel: ");
        str = str_trim(get_str());
        if (strncasecmp(str, "y", 1) == 0) {
            list_removematch(l, comp_id, &stu);
            printf("Record was removed, press enter to continue... ");
            get_str();
        }
    } else {
        printf("Record not found, press enter to continue... ");
        get_str();
    }
    disp_menu();
    select_menu(l);
}

/* 从文件中导入记录        */
bool import_data(LIST l, char *path)
{
    char *str;
    char **tmp;
    struct student stu;
    int id;
    char *name, *gender, *depart;

    if (!(file_open_r(path)))
        return false;
    while ((str = read_line()) != NULL) {
        if (split_num(str, ",") == 4) {
            tmp = &str;
            id = atoi(strsep(tmp, ","));
            name = str_trim(strsep(tmp, ","));
            gender = str_trim(strsep(tmp, ","));
            depart = str_trim(strsep(tmp, ","));

            stu = put_data(id, name, gender, depart);
            list_addlast(l, &stu);
        }
    }

    file_close();
    return true;
}

/* 将给定字符串按行写入文件, 作为回调函数使用        */
static void write_data(void *data)
{
    char tmp[MAX_LEN];
    struct student *stu = data;

    sprintf(tmp, "%d,%s,%s,%s", stu->id, stu->name,
            stu->gender, stu->depart);
    write_line(tmp);
}

/* 遍历结构, 并执行回调函数, 将记录写入文件        */
bool export_data(LIST l, char *path)
{
    if (!(file_open_w(path)))
        return false;
    list_travel(l, write_data);
    
    file_close();
    return true;
}

/* 将全部记录写入文件        */
bool save_manual(LIST l)
{
    if (!(export_data(l, PATH)))
        return false;
    printf("All records were saved.\n");
    printf("Press enter to continue... ");
    get_str();
    disp_menu();
    select_menu(l);

    return true;
}