/* ********************************************************　
 * name:        add.c
 * author:      Joshua Chan
 * description: 对二个随意长度的整数进行加法运算
                以程序参数的形式输入二个随意长度整数（可为负整数）
                程序会输出其加法运算的结果
 * *******************************************************/

#include <stdio.h>
#include <string.h>

#define MAX_LEN 80

/* 将字符 '0'~'9' 转化为数字 0~9 */
static int ctoi(char c)
{
    return (c - '0');
}

/*
 * 实现二个整数字符串的加法运算，要求二整数符号相同
 */
static void _add(const char *num1, const char *num2, char *result)
{
    int i, j, k, n1, n2, n;
    int len1, len2;
    int sig;
    int sta;
    /* 进位标识 */
    int car;
    char c[MAX_LEN] = {0};

    /* 判断输入整数的正负号 */
    sig = (*num1 == '-') ? -1 : 1;
    /* 判断有效数字起始位置 */
    sta = (sig == -1) ? 1 : 0;
    len1 = strlen(num1);
    len2 = strlen(num2);

    /*
     * 对二整数自低位起进行逐位相加，逢十进位，计算结果逐位存入临时数组
     * 比较二整数长度，以大者长度作为循环相加次数
     */
    if (len1 >= len2) {
        j = len2 - 1;
        k = 0;
        car = 0;
        for (i = len1-1; i >= sta; i--) {
            n1 = ctoi(num1[i]);
            n2 = (j >= sta) ? (ctoi(num2[j])) : 0;
            j--;
            n = (car == 1) ? (n1 + n2 + 1) : (n1 + n2);
            if (n > 9) {
                n = n % 10;
                car = 1;
            }
            else
                car = 0;
            c[k] = n + '0';
            k++;
        }
        if (car == 1) {
            c[k] = 1 + '0';
            k++;
        }
    }
    else {
        j = len1 -1;
        k = 0;
        car = 0;
        for (i = len2-1; i >=sta; i--) {
            n2 = ctoi(num2[i]);
            n1 = (j >= sta) ? (ctoi(num1[j])) : 0;
            j--;
            n = (car == 1) ? (n1 + n2 + 1) : (n1 + n2);
            if (n > 9) {
                n = n % 10;
                car = 1;
            }
            else
                car = 0;
            c[k] = n + '0';
            k++;
        }
        if (car == 1) {
            c[k] = 1 + '0';
            k++;
        }
    }

    /* 将临时数组内容逆序取出 */
    i = 0;
    if (sig == -1) {
        result[i] = '-';
        i++;
    }
    for (j = k-1; j >= 0; j--) {
        result[i] = c[j];
        i++;
    }
}

/*
 * 实现二个整数字符串的减法运算，即求(num1 - num2)的值
 * 要求num1绝对值大于num2
 */
static void _sub(const char *num1, const char *num2, char *result)
{
    int i, j, k, n1, n2, n;
    int len1, len2;
    /* 以num1符号作为结果的符号 */
    int sig;
    int sta1, sta2;
    /* 借位标识 */
    int car;
    char c[MAX_LEN] = {0};

    sig = (*num1 == '-') ? -1 : 1;
    sta1 = (*num1 == '-') ? 1 : 0;
    sta2 = (*num2 == '-') ? 1 : 0;
    len1 = strlen(num1);
    len2 = strlen(num2);

    j = len2 - 1;
    k = 0;
    car = 0;
    /*
     * 对二整数自低位起进行逐位相减，结果小于0则借位再减，计算结果逐位存入临时数组
     */
    for (i = len1-1; i >= sta1; i--) {
        n1 = ctoi(num1[i]);
        n2 = (j >= sta2) ? (ctoi(num2[j])) : 0;
        j--;
        if (car == 1) {
            n1 = n1 - 1;
            car = 0;
        }
        if (n1 < n2) {
            n = n1 + 10 - n2;
            car = 1;
        }
        else
            n = n1 - n2;
        c[k] = n + '0';
        k++;
    }

    /* 将临时数组内容逆序取出 */
    i = 0;
    if (sig == -1) {
        result[i] = '-';
        i++;
    }
    for (j = k-1; j >= 0; j--) {
        result[i] = c[j];
        i++;
    }
}

static void add(const char *num1, const char *num2, char *result)
{
    int i, j;
    int bigger;
    int len1, len2;
    int sig1, sig2;
    int sta1, sta2;

    /* 判断输入整数的正负号 */
    sig1 = (*num1 == '-') ? -1 : 1;
    sig2 = (*num2 == '-') ? -1 : 1;
    len1 = strlen(num1);
    len2 = strlen(num2);
    /* 判断有效数字起始位置 */
    sta1 = (sig1 == -1) ? 1 : 0;
    sta2 = (sig2 == -1) ? 1 : 0;

    /*
     * 判断二整数符号与绝对值
     * 先判断符号，若符号相同则 bigger = -1
     * 否则：
     * bigger = 1: 表示整数1绝对值大
     * bigger = 2: 表示整数2绝对值大
     */
    bigger = 0;
    if (sig1 == sig2)
        bigger = -1;
    else if ((len1 - sta1) > (len2 - sta2))
        bigger = 1;
    else if ((len1 - sta1) < (len2 - sta2))
        bigger = 2;
    else {
        for (i = sta1; i < len1; i++) {
            j = sta2;
            if (num1[i] > num2[j]) {
                bigger = 1;
                break;
            }
            else if (num1[i] < num2[j]) {
                bigger = 2;
                break;
            }
            j++;
        }
    }

    /*
     * 根据bigger值确定计算方式
     * bigger = 0: 二整数符号相异而绝对值相同，直接返回'0'
     * bigger = -1: 二整数符号相同，用加法计算
     * bigger = 1: 二整数符号相异，且整数1绝对值大，用减法计算，整数1作为被减数
     * bigger = 2: 二整数符号相异，且整数2绝对值大，用减法计算，整数2作为被减数
     */
    switch (bigger) {
    case 0:
        result[0] = '0';
        break;
    case -1:
        _add(num1, num2, result);
        break;
    case 1:
        _sub(num1, num2, result);
        break;
    case 2:
        _sub(num2, num1, result);
        break;
    default:
        break;
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        printf("Usage: %s num1 num2\n", argv[0]);
        return -1;
    }
    
    char s[MAX_LEN] = {0};

    add(argv[1], argv[2], s);
    printf("The result is: %s\n", s);

    return 0;
}