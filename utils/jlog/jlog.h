/* ***************************************************************
 * File Name: jlog.h
 * Function:  实现日志记录功能
 * Description: 主要功能:
 *              1. 支持在信息的开始位置指定信息级别, 未指定则会认为是默认级别
 *              2. 支持四种模式:
 *                 日志关闭/写入文件/控制台显示/同时写入文件和控制台显示
 * Author & Date: Joshua Chan, 2012/01/01
 * ***************************************************************/
#ifndef _JLOG_H
#define _JLOG_H

/* 日志文件路径 */
#define LOGFILE_PATH  "./log"

/* 信息级别前缀, 可在信息开始位置加入 */
#define J_FATAL    "<0>"
#define J_ERR      "<1>"
#define J_WARN     "<2>"
#define J_INFO     "<3>"
#define J_DEBUG    "<4>"

/* 信息标志文字, 根据信息级别自动加入进日志文件 */
#define FLAG_FATAL "[fatal]"
#define FLAG_ERR   "[error]"
#define FLAG_WARN  "[warn]"
#define FLAG_INFO  "[info]"
#define FLAG_DEBUG "[debug]"

/* 未指明级别的信息处理方式 */
#define INDEX_DEFAULT INDEX_INFO

/* 信息级别对应的编号 */
enum prior_index {
    INDEX_NONE = -1,
    INDEX_FATAL,
    INDEX_ERR,
    INDEX_WARN,
    INDEX_INFO,
    INDEX_DEBUG,
};

/* 四种日志模式 */
enum jlog_mode {
    JLOG_OFF = -1,        //日志关闭
    JLOG_TO_FILE,         //仅写入文件
    JLOG_TO_CONSOLE,      //控制台显示
    JLOG_TO_FILE_CONSOLE, //写入文件且在控制台显示
};

/* 日志模式标志, 可以取 enum jlog_mode{} 中的值
   默认为JLOG_TO_FILE, 信息仅存入日志文件
*/
extern int jlog_mode;

/* 将信息存入日志文件
   传入参数的开始位置可以指定信息级别, 未指定级别将转为默认级别
   例如: jlog("%m", errno); 将出错信息以默认级别写入日志文件
   函数返回值为实际写入的字符数
*/
extern int jlog(char *fmt, ...);

#define JLOG(fmt, args...)                              \
    do {                                                \
        int mode = jlog_mode;                           \
        if (mode == JLOG_TO_FILE_CONSOLE                \
            || mode == JLOG_TO_CONSOLE )                \
            printf("[%s:%d]", __FUNCTION__, __LINE__);  \
        jlog(fmt, args);                                \
    } while (0)                                         \

#endif