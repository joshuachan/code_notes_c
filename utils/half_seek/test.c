/* �۰��������Գ��� */
#include <stdio.h>
#include <stdlib.h>

extern int half_seek(int x, int arr[], int num);
static void print_array(int arr[], int num);

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage: %s number\n", argv[0]);
        return -1;
    }

    int arr[] = {1, 2, 3, 4};

    print_array(arr, sizeof(arr)/sizeof(int));
    printf("search result: %d\n",
           half_seek(atoi(argv[1]), arr, sizeof(arr)/sizeof(int)));
    return 0;
}

static void print_array(int arr[], int num)
{
    int i;

    for (i = 0; i < num; i++)
        printf("%d ", arr[i]);
    putchar('\n');
}