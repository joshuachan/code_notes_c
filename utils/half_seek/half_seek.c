/* �۰����� */
int half_seek(int x, int arr[], int num)
{
    int low, high, mid;

    low = 0;
    high = num - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < arr[mid])
            high = mid - 1;
        else if (x > arr[mid])
            low = mid + 1;
        else
            return mid;
    }
    return -1;
}