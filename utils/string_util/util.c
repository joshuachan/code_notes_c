/* *************************************
 * Name: util.c
 * Desc: 有关字符串的一些处理函数
 *       包括: 输入/分割检测/去空格
 *       示例代码请见<简单信息管理系统>
 * Author & Date: Joshua Chan, 2011/11/08
 * *************************************/
#include <string.h>
#include <stdio.h>
#include "main.h"

char s[MAX_LEN];

/* 将字符串str2插入至str1的pos位置 */
char *str_insert(char *str1, const char *str2, int pos)
{
    int len1;
    int len2;

    len1 = strlen(str1);
    len2 = strlen(str2);
    /* 将str1自pos位置开始向后移动len(str2)位置 */
    memmove(str1+pos+len2, str1+pos, len1-pos+1);
    memcpy(str1+pos, str2, len2);

    return str1;
}

/* 计划在程序中实现类似"Press enter to continue..."的功能
 * 但发现scanf()不能接受空字符串, 只好改用fgets()实现
 * 并且用来接受标准输入也很方便, 可以规定最大读取长度 */
inline char *get_str(void)
{
    fgets(s, MAX_LEN - 1, stdin);
    s[strlen(s)-1] = '\0';
    return s;
}

/* 计算字符串被delim分割后的段数
 * 可用来判断给定的字符串所包含的字段数量是否与期望的一致    */
int split_num(char *s, const char *delim)
{
    int n = 1;
    char *tmp = s;

    while ((tmp = strstr(tmp, delim)) != NULL) {
        tmp++;
        n++;
    }

    return n;
}

/* 简单的trim()功能, 去掉字符串头尾的空白    */
char *str_trim(char *s)
{
    int i = 0;
    int j = 0;
    int k;
    int len = strlen(s);
    char *tmp = s;
    
    while ((tmp[i] == ' ') || (tmp[i] == '\t') || (tmp[i] == '\n'))
        i++;
    if (i == len) {
        s[0] = '\0';
    } else {
        while ((tmp[len-1-j] == ' ') || (tmp[len-1-j] == '\t')
                    || (tmp[len-1-j] == '\n'))
            j++;
        for (k = 0; k < len-i-j; k++)
            s[k] = s[k+i];
        s[k] = '\0';
    }

    return s;
}