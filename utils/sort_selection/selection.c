/* **************************************
 * Name: selection.c
 * Description: ʵ��ѡ������
 * Author & Date: Joshua Chan 2011/11/11
 * *************************************/

void selection_sort(int *array, int size)
{
int i, j;
int p, tmp;

for (i = 0; i < size-1; i++) {
p = i;
for (j = i+1; j < size; j++) {
if (array[j] < array[p])
p = j;
}
tmp = array[p];
array[p] = array[i];
array[i] = tmp;
}
}