/* **************************************
 * Name: test.c
 * Description: ������Գ���
 * Author & Date: Joshua Chan 2011/11/11
 * *************************************/
#include <stdio.h>
void print_array(int *array, int size);

void insertion_sort(int *array, int size);
void bubble_sort(int *array, int size);
void selection_sort(int *array, int size);

int n[9] = {6, 9, 5, 2, 8, 4, 1, 3, 7};

int main(void)
{
    printf("The original array is: ");
    print_array(n, 9);
    //selection_sort(n, 9);
    bubble_sort(n, 9);
    //insertion_sort(n, 9);
    printf("The sorted array is:   ");
    print_array(n, 9);

    return 0;
}

void print_array(int *array, int size)
{
    int i = 0;

    while (i < size)
        printf("%d, ", array[i++]);
    putchar('\n');
}