/* **********************************************************************
 * comment_converter: 转换C代码注释风格, 将C++风格注释转换为传统C风格注释
 *                    需转换的文件名以参数形式传入
 * author & date: Joshua Chan, joshuachan@yeah.net, 2012/05/09
 * **********************************************************************/
#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define LINE_SIZE 1024

/* 将字符串str2插入至str1的pos位置 */
char *str_insert(char *str1, const char *str2, int pos)
{
    int len1;
    int len2;

    len1 = strlen(str1);
    len2 = strlen(str2);
    /* 将str1自pos位置开始向后移动len(str2)位置 */
    memmove(str1+pos+len2, str1+pos, len1-pos+1);
    memcpy(str1+pos, str2, len2);

    return str1;
}

/* 检查字符串, 确保以换行符结尾 */
void add_newline(char *str)
{
    int len;

    len = strlen(str);
    if (str[len-1] != '\n')
        str[len-1] = '\n';
}

/* 增加注释头, 确保注释头后跟一空格, @comment_pos为注释开始位置 */
void add_comment_head(char *str, int comment_pos)
{
    str[comment_pos+1] = '*';
    if (str[comment_pos+2] != ' ' && str[comment_pos+2] != '\t')
        str_insert(str, " ", comment_pos+2);
}

/* 增加注释尾, 确保注释尾前有一空格 */
void add_comment_tail(char *str)
{
    int len;

    add_newline(str);
    len = strlen(str);
    str[len-1] = 0;
    if (str[len-2] != ' ' && str[len-2] != '\t')
        strcat(str, " ");
    strcat(str, "*/\n");
}

/* 将C++风格注释转换为传统C风格注释
 * 需转换的文件名以参数形式传入
 */
int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: %s <file_to_be_converted>\n", argv[0]);
        return -1;
    }

    int i;
    FILE *fd_rd;
    FILE *fd_wr;
    char tmpfname[16];
    bool in_string;             /* 标记当前是否处于字符串中 */
    bool in_comment;            /* 标记当前是否处于传统C风格注释中 */
    bool find_comment;          /* 标记是否找到C++风格注释 */
    char line[LINE_SIZE];

    memset(tmpfname, 0, sizeof(tmpfname));
    memset(line, 0, sizeof(line));
    /* 生成临时文件名 */
    tmpnam(tmpfname);
    str_insert(tmpfname, ".", 0);
    fd_rd = fopen(argv[1], "r");
    fd_wr = fopen(tmpfname, "w");

    while (fgets(line, LINE_SIZE, fd_rd)) {
        in_string = false;
        find_comment = false;
        for (i = 0; line[i] != 0; i++) {
            /* 检查当前是否处于字符串中 */
            if (line[i] == '\"') {
                in_string = in_string ? false : true;
            } else if (line[i] == '/') {
                /* 检查当前是否进入传统C风格注释 */
                if (line[i+1] == '*') {
                    in_comment = true;
                } else if (line[i+1]=='/' && !in_string && !in_comment) { /* 检查是否为C++风格注释 */
                    find_comment = true;
                    break;
                }
            } else if (line[i] == '*') { /* 检查当前是否退出传统C风格注释 */
                if (line[i+1]=='/' && line[i-1]!='/')
                    in_comment = false;
            }
        }

        if (find_comment) {
            add_comment_head(line, i);
            add_comment_tail(line);
        }
        fputs(line, fd_wr);
    }

    fclose(fd_rd);
    fclose(fd_wr);
    remove(argv[1]);
    rename(tmpfname, argv[1]);

    return 0;
}
