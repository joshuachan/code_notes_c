#include "uart.h"
#include "protocol.h"

static u8 rx_pack[PACK_MAX_LENGTH]; /* 数据接收缓冲 */
static u8 rx_checksum;              /* 最后接收数据的校验值 */
static u8 tx_checksum;              /* 最后发送数据的校验值 */

/* 按协议格式发送数据包, 参数中传入command, length, data的值 */
void pack_send(cmd1 command1, cmd2 command2, u8 length, const void *data)
{
	u8 i;
	u8 *tmp;

	tmp = (u8 *)data;
	tx_checksum = 0;            /* 每次发送需从新计算校验值 */
	uart_putchar(STX1);
	uart_putchar(STX2);
	uart_putchar(command1);
	uart_putchar(command2);
	uart_putchar(length);
	tx_checksum += ((u8)command1 + (u8)command2 + length);
	for (i = 0; i < length; i++) {
		uart_putchar(tmp[i]);
		tx_checksum += tmp[i];
	}
	uart_putchar(tx_checksum);
	uart_putchar(ETX1);
	uart_putchar(ETX2);
}

/* 解析传入的队列, 要求传入的队列中含有'STX'及'ETX'标志 */
bool pack_decode(queue_st *q)
{
	u8 i;
	u8 length;
	bool pack_found;

	rx_checksum = 0;            /* 每次接收需从新计算校验值 */
	pack_found = false;         /* 初始化解析成功标志 */

	/* 队列数据长度不可小于最小协议包长度 */
	while (q->data_size >= PACK_HEAD_LENGTH + PACK_TAIL_LENGTH) {
		/* 寻找STX标志 */
		if (queue_get_index(q, PACK_STX1_OFFSET) == STX1
				&& queue_get_index(q, PACK_STX2_OFFSET) == STX2) {
			/* 找到STX标志后, 即可得到length值 */
			length = queue_get_index(q, PACK_LENGTH_OFFSET);
			/* 队列数据长度不可小于根据length值求出的协议包长度 */
			if (q->data_size >= PACK_HEAD_LENGTH + length + PACK_TAIL_LENGTH) {
				/* 寻找ETX标志 */
				if (queue_get_index(q, length+PACK_ETX1_OFFSET) == ETX1
						&& queue_get_index(q, length+PACK_ETX2_OFFSET) == ETX2) {
					/* 找到ETX标志后, 求出checksum值 */
					for (i = 0; i < PACK_LENGTH_OFFSET-PACK_STX2_OFFSET+length; i++)
						rx_checksum += queue_get_index(q, i + PACK_COMMAND1_OFFSET);
					/* checksum值比对 */
					if (rx_checksum ==
							queue_get_index(q, length+PACK_CHECKSUM_OFFSET)) {
						/* checksum值匹配, 则解析成功 */
						pack_found = true;
						/* 将command1, command2, length, data复制到接收缓冲 */
						for (i = 0; i < PACK_LENGTH_OFFSET-PACK_STX2_OFFSET+length; i++)
							rx_pack[i] = queue_get_index(q, i + PACK_COMMAND1_OFFSET);
						/* 将已解析的协议包数据标记为已读 */
						queue_mark_get(q, PACK_HEAD_LENGTH+length+PACK_TAIL_LENGTH);
						break;
					} else {
						/* checksum不匹配, 抛弃此STX标志 */
						queue_get(q);
						queue_get(q);
					}
				} else {
					/* 规定位置未发现ETX标志, 抛弃此STX标志 */
					queue_get(q);
					queue_get(q);
				}
			} else {
				/* 队列数据长度小于协议包长度, 抛弃此STX标志 */
				queue_get(q);
				queue_get(q);
			}
		} else {
			/* 队列数据开始位置不是STX标志, 抛弃之 */
			queue_get(q);
			queue_get(q);
		}
	}

	return pack_found;
}

/* 获取最后接收数据包的command1值 */
u8 pack_get_command1(void)
{
	return rx_pack[0];
}

/* 获取最后接收数据包的command2值 */
u8 pack_get_command2(void)
{
	return rx_pack[1];
}

/* 获取最后接收数据包的length值 */
u8 pack_get_length(void)
{
	return rx_pack[2];
}

/* 获取最后接收数据包的data数据 */
u8 *pack_get_data(void)
{
	return rx_pack + 3;
}

/* 获取最后接收数据包的checksum值 */
u8 pack_get_rx_checksum(void)
{
	return rx_checksum;
}

#if 0
/* 接收方在收到一条非ACK指令时, 会将此指令中的checksum存入data段,
 * 并回复一条ACK指令, 发送方在收到此ACK指令时, 需将此指令data段中
 * 的checksum与上次发送指令的checksum比较, 以确定是否需要从新发送
 */
bool ack_checksum_verify(void)
{
	return (rx_checksum == tx_checksum);
}

/* 获取最后发送数据包的checksum值 */
u8 pack_get_tx_checksum(void);
{
	return tx_checksum;
}
#endif
