/* *******************************************
 * Name: stack.h
 * Desc: 顺序栈的应用实现
 *       包括: 初始化/销毁/入栈/出栈/遍历等操作
 * Author: Joshua Chan
 * Date: 2011-11-4
 * *******************************************/
#ifndef _STACK_H_
#define _STACK_H_

#define STEP    3   /* 空间增长步进值    */

typedef int data_t;

struct stack {
    data_t *data;
    int dsize;      /* 数据元素大小      */
    int max;        /* 当前可容纳记录数  */
    int top;        /* 栈顶指针          */
};

/* 操作函数                              */
int stack_init(struct stack *s);
void stack_release(struct stack *s);
bool stack_empty(struct stack *s);
bool stack_full(struct stack *s);
int stack_push(struct stack *s, data_t *data);
int stack_pop(struct stack *s, data_t *data);
void print_data(data_t *data);
int stack_print(struct stack *s);
void put_data(data_t *data, int n);

#endif