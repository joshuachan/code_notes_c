顺序栈结构的实现程序

　　简单顺序栈结构的实现代码，包括: 初始化/销毁/入栈/出栈/遍历等操作。
　　共3个文件，头文件<stack.h>，函数实现<stack.c>，测试程序<test.c>。