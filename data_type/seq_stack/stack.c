/* *******************************************
 * Name: stack.c
 * Desc: 顺序栈的应用实现
 *       包括: 初始化/销毁/入栈/出栈/遍历等操作
 * Author: Joshua Chan
 * Date: 2011-11-4
 * *******************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "stack.h"

/* 结构初始化, 预分配数据存储空间    */
int stack_init(struct stack *s)
{
    s->dsize = sizeof(data_t);
    s->max = STEP;
    s->top = 0;
    if ((s->data = (data_t *)malloc(s->max * s->dsize)) == NULL)
        return -1;
    return 0;
}

/* 空间释放            */
void stack_release(struct stack *s)
{
    s->max = 0;
    s->top = 0;
    free(s->data);
}

/* 检测非空            */
bool stack_empty(struct stack *s)
{
    return s->top == 0 ? true : false;
}

/* 检测非满            */
bool stack_full(struct stack *s)
{
    return s->top == s->max ? true : false;
}

/* 数据入栈            */
int stack_push(struct stack *s, data_t *data)
{
    if (stack_full(s) == true) {
        s->max += STEP;
        if(( s->data = (data_t *)realloc(s->data, s->max * s->dsize)) == NULL)
            return -1;
    }

    //s->data[s->top++] = *data;
    memcpy(&s->data[s->top++], data, s->dsize);

    return 0;
}

/* 数据出栈            */
int stack_pop(struct stack *s, data_t *data)
{
    if (stack_empty(s) == true)
        return -1;

    //*data = s->data[--s->top];
    memcpy(data, &s->data[--s->top], s->dsize);

    return 0;
}

void print_data(data_t *data)
{
    printf("%d, ", *data);
}

/* 遍历栈数据        */
int stack_print(struct stack *s)
{
    int i;

    if (stack_empty(s) == true)
        return -1;

    i = s->top;
    while (i > 0)
        print_data(&s->data[--i]);
    putchar('\n');

    return 0;
}

/* 对待加入元素赋值    */
void put_data(data_t *data, int n)
{
    //*data = n;
    memcpy(data, &n, sizeof(data_t));
}