/* ************************************************
 * Name: list.c
 * Desc: ADT(抽象数据类型)线性存储C语言描述
 *       实现的操作:
 *       初始化/销毁/指定位置追加/查找符合条件记录
 *        /指定位置删除/有序表中自动寻找匹配位置追加
 *        /指定位置读取/按给定条件排序等
 * Author & Date: Joshua Chan, 2011/11/17
 * ************************************************/ 
#include <string.h>
#include <stdlib.h>
#include "list.h"

/* 结构非空判断        */
inline static bool list_empty(LIST l)
{
    return (l->count == 0) ? true : false;
}

/* 结构初始化        */
LIST list_init(int elemsize)
{
    int i;

    /* 为结构分配空间            */
    LIST l = malloc(sizeof(struct list));
    if (l == NULL)
        return NULL;

    l->elemsize = elemsize;
    /* 为指针数组分配空间        */
    l->data = malloc(sizeof(void *) * INIT_SIZE);
    /* 预分配用户数据存储空间    */
    for (i = 0; i < INIT_SIZE; i++)
        l->data[i] =  malloc(l->elemsize);
    l->count = 0;
    l->loads = INIT_SIZE;

    return l;
}

/* 结构销毁        */
bool list_release(LIST l)
{
    int i;

    for (i = 0; i < l->count; i++)
        free(l->data[i]);
    free(l->data);
    free(l);

    return true;
}

/* 空间扩展        */
inline static bool list_expand(LIST l) {
    int i;

    l->data = realloc(l->data, sizeof(void *) * (l->loads + STEP_SIZE));
    if (l->data == NULL)
        return false;
    for (i = 0; i < STEP_SIZE; i++)
        l->data[l->loads + i] = malloc(l->elemsize);
    l->loads += STEP_SIZE;

    return true;
}

/* 指定位置追加        */
bool list_addat(LIST l, int index, void *data)
{
    int i;
    int *tmp;

    if (index < 0 || index > l->count)
        return false;

    if (l->count == l->loads) {
        if (list_expand(l) == false)
            return false;
    }

    /* 数据后移, 为新元素准备位置        */
    tmp = (int *)l->data[l->count];
    for (i = l->count; i > index; i--)
        l->data[i] = l->data[i-1];
    l->data[index] = (void *)tmp;
    memcpy(l->data[index], data, l->elemsize);
    l->count++;
    
    return true;
}

/* 尾部追加        */
bool list_addlast(LIST l, void *data)
{
    return list_addat(l, l->count, data);
}

/* 头部追加        */
bool list_addfirst(LIST l, void *data)
{
    return list_addat(l, 0, data);
}

/* 指定位置删除        */
bool list_removeat(LIST l, int index)
{
    int i;
    int *tmp;

    if (list_empty(l))
        return false;
    if (index < 0 || index >= l->count)
        return false;

    tmp = (int *)l->data[index];
    for (i = index; i < l->count - 1; i++)
        l->data[i] = l->data[i+1];
    l->data[l->count - 1] = (void *)tmp;
    l->count--;

    return true;
}

/* 删除尾部        */
bool list_removelast(LIST l)
{
    return list_removeat(l, l->count - 1);
}

/* 删除头部        */
bool list_removefirst(LIST l)
{
    return list_removeat(l, 0);
}

/* 结构遍历, 并执行指定操作        */
bool list_travel(LIST l, proc_func proc)
{
    int i;

    if (list_empty(l))
        return false;
    for (i = 0; i < l->count; i++)
        proc(l->data[i]);

    return true;
}

/* 寻找匹配位置追加        */
bool list_addmatch(LIST l, comp_func comp, void *data, bool ord)
{
    int i;
    bool sig;

    if (list_empty(l))
        return list_addlast(l, data);

    for (i = 0; i < l->count; i++) {
        sig = (comp(l->data[i], data) > 0) ? true : false;
        if (sig == ord)
            break;
    }

    return list_addat(l, i, data);
}

/* 删除匹配记录        */
bool list_removematch(LIST l, comp_func comp, void *data)
{
    int i;
    bool sig;

    if (list_empty(l))
        return false;

    sig = false;
    for (i = 0; i < l->count; i++) {
        if (comp(l->data[i], data) == 0)
            return list_removeat(l, i);
    }

    return false;
}

/* 指定位置读取        */
void *list_getat(LIST l, int index)
{
    if (list_empty(l))
        return NULL;
    if (index < 0 || index >= l->count)
        return NULL;

    return l->data[index];
}

/* 读取匹配记录        */
void *list_getmatch(LIST l, comp_func comp, void *data)
{
    int i;

    if (list_empty(l))
        return NULL;
    for (i = 0; i < l->count; i++) {
        if (comp(l->data[i], data) == 0)
            return l->data[i];
    }

    return NULL;
}

/* 按指定排序依据用插入法排序        */
bool list_sort(LIST l, comp_func comp, bool ord)
{
    int i, j, *tmp;
    bool sig;

    if (list_empty(l))
        return false;
    for (i = 1; i < l->count; i++) {
        tmp = (int *)l->data[i];
        for (j = i - 1; j >= 0; j--) {
            sig = (comp(l->data[j], tmp) > 0) ? true : false;
            if (sig == ord)
                l->data[j+1] = l->data[j];
            else
                break;
        }
        l->data[j+1] = (void *)tmp;
    }

    return true;
}