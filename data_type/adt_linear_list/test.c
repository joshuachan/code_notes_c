/* ************************************************
 * Name: list.h
 * Desc: ADT(抽象数据类型)线性存储测试程序
 * Author & Date: Joshua Chan, 2011/11/17
 * ************************************************/ 
#include <stdio.h>
#include <string.h>
#include "list.h"

/* 用户自定论数据结构        */
struct info {
    int id;
    char name[20];
    int age;
};

/* 用户定义比较函数        */
int comp_id(void *data1, void *data2)
{
    struct info *a = data1;
    struct info *b = data2;

    if (a->id == b->id)
        return 0;
    return (a->id > b->id) ? 1 : -1;
}

int comp_name(void *data1, void *data2)
{
    struct info *a = data1;
    struct info *b = data2;

    return strcmp(a->name, b->name);
}

int comp_age(void *data1, void *data2)
{
    struct info *a = data1;
    struct info *b = data2;

    if (a->age == b->age)
        return 0;
    return (a->age > b->age) ? 1 : -1;
}

/* 用户自定义方法函数        */
void proc_print(void *data)
{
    struct info *a = data;

    printf("%d\t%s\t%d\n", a->id, a->name, a->age);
}

void print_title(void)
{
    printf("ID\tName\tAge\n");
    printf("====================\n");
}

/* 根据指定参数生成用户结构        */
struct info put_data(int id, char *name, int age)
{
    struct info new;

    new.id = id;
    strcpy(new.name, name);
    new.age = age;

    return new;
}

/* 测试程序        */
int main(void)
{
    LIST l;
    struct info i;

    l = list_init(sizeof(struct info));

    /* 追加五条记录        */
    i = put_data(3, "Zhao", 20);
    list_addlast(l, &i);
    i = put_data(5, "Qian", 43);
    list_addlast(l, &i);
    i = put_data(7, "Sun", 3);
    list_addlast(l, &i);
    i = put_data(9, "Li", 14);
    list_addlast(l, &i);
    i = put_data(11, "Zhou", 28);
    list_addlast(l, &i);

    /* 遍历结构, 打印数据信息        */
    printf("Add 5 records.\n");
    print_title();
    list_travel(l, proc_print);

    /* 在头部加入一条记录        */
    i = put_data(4, "Wu", 59);
    list_addfirst(l, &i);
    printf("\nAdd a record on head.\n");
    print_title();
    list_travel(l, proc_print);

    /* 在指定位置加入一条记录        */
    i = put_data(2, "Zheng", 33);
    list_addat(l, 2, &i);
    printf("\nAdd a record on pos 2.\n");
    print_title();
    list_travel(l, proc_print);

    /* 读取指定位置记录        */
    i = *(struct info *)list_getat(l, 3);
    printf("\nGet record on pos 3.\n");
    print_title();
    proc_print(&i);

    /* 读取符合条件的记录        */
    i = put_data(0, "Zhao", 0);
    i = *(struct info *)list_getmatch(l, comp_name, &i);
    printf("\nSearch record which named Zhao.\n");
    print_title();
    proc_print(&i);

    /* 按name升序排列        */
    list_sort(l, comp_name, true);
    printf("\nSorted by name with ascending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 按age降序排列        */
    list_sort(l, comp_age, false);
    printf("\nSorted by age with descending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 寻找匹配位置加入记录        */
    i = put_data(1, "Wang", 30);
    list_addmatch(l, comp_age, &i, false);
    printf("\nAuto put the new record on the proper pos.\n");
    print_title();
    list_travel(l, proc_print);
    
    /* 按id升序排列        */
    list_sort(l, comp_id, true);
    printf("\nSorted by id with ascending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 删除指定位置记录        */
    list_removeat(l, 3);
    printf("\nRemove record on pos 3.\n");
    print_title();
    list_travel(l, proc_print);

    /* 删除符合条件的记录        */
    i = put_data(0, "Li", 0);
    list_removematch(l, comp_name, &i);
    printf("\nRemove record which named Li.\n");
    print_title();
    list_travel(l, proc_print);

    list_release(l);
    return 0;
}