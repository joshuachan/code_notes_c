/* **************************************************** 
 * main.c: 循环队列模块
 * Function: 实现循环队列的数据存取
 * Authon: Joshua Chen <joshuachan@yeah.net> 2012/04/13
 * ***************************************************/
#include <stdio.h>
#include "queue.h"

/* 测试程序 */
int main(void)
{
    u8 s[] = "Hello, this is a test string.";
    u8 *p = s;
    queue_st q;
    u8 queue_len;
    
    queue_len = 8;
    q = queue_init(queue_len);
    while (*p) {
        /* 注意队列数据的存取前均需进行队列状态的判断 */
        if (queue_not_full(q))
            queue_put(q, *p++);
        if (queue_not_empty(q))
            putchar(queue_get(q));
    }
    return 0;
}
