/* **************************************************** 
 * circular_queue.c: 循环队列模块
 * Function: 实现循环队列的数据存取
 * Authon: Joshua Chen <joshuachan@yeah.net> 2012/04/13
 * ***************************************************/
#include <stdlib.h>
#include "queue.h"

/* 队列非满, 存数据前需作此判断 */
bool queue_not_full(queue_st q)
{
    return ((q->size < q->load) ? true : false);
}

/* 队列非空, 取数据前需作此判断 */
bool queue_not_empty(queue_st q)
{
    return ((q->size > 0) ? true : false);
}

/* 队列初始化 */
queue_st queue_init(u8 load)
{
    queue_st q;

    q = malloc(sizeof(struct queue_struct));
    q->data = malloc(load);
    q->put_pos = 0;
    q->get_pos = 0;
    q->load = load;
    q->size = 0;
    return q;
}

/* 队列删除 */
void queue_release(queue_st q)
{
    free(q->data);
    free(q);
}

/* 存数据, 需先判断队列非满 */
void queue_put(queue_st q, u8 c)
{
    q->data[q->put_pos++] = c;
    if (q->put_pos >= q->load)
        q->put_pos = 0;
    q->size++;
}

/* 取数据, 需先判断队列非空 */
u8 queue_get(queue_st q)
{
    u8 ret;

    ret = q->data[q->get_pos++];
    if (q->get_pos >= q->load)
        q->get_pos = 0;
    q->size--;
    return ret;
}
