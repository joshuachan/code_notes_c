/* **************************************************** 
 * circular_queue.h: 循环队列模块
 * Function: 实现循环队列的数据存取
 * Authon: Joshua Chen <joshuachan@yeah.net> 2012/04/13
 * ***************************************************/
#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdbool.h>
#include "hardware.h"

/* 定义队列结构 */
typedef struct queue_struct {
    u8 *data;                   /* 数据存储空间 */
    u8 put_pos;                 /* 存数据指针 */
    u8 get_pos;                 /* 取数据指针 */
    u8 load;                    /* 数据空间容量 */
    u8 size;                    /* 当前数据长度 */
} *queue_st;

/* 队列非满, 存数据前需作此判断 */
bool queue_not_full(queue_st q);

/* 队列非空, 取数据前需作此判断 */
bool queue_not_empty(queue_st q);

/* 队列初始化 */
queue_st queue_init(u8 load);

/* 存数据, 需先判断队列非满 */
void queue_put(queue_st q, u8 c);

/* 取数据, 需先判断队列非空 */
u8 queue_get(queue_st q);

#endif
