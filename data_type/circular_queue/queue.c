/* **************************************************** 
 * circular_queue.c: 循环队列模块
 * Function: 实现循环队列的数据存取
 * Authon: Joshua Chen <joshuachan@yeah.net> 2012/04/13
 * ***************************************************/
#include <stdlib.h>
#include "queue.h"

/* 队列非满, 存数据前需作此判断 */
bool queue_not_full(queue_st *q)
{
    return ((q->data_size < q->buf_size) ? true : false);
}

/* 队列非空, 取数据前需作此判断 */
bool queue_not_empty(queue_st *q)
{
    return ((q->data_size > 0) ? true : false);
}

/* 队列初始化 */
void queue_init(queue_st *q, u8 *buf, u8 buf_size)
{
    q->buf = buf;
    q->buf_size = buf_size;
    q->data_size = 0;
    q->data_index = 0;
}

/* 存数据, 需先判断队列非满 */
void queue_put(queue_st *q, u8 c)
{
    q->buf[(q->data_index + q->data_size) % q->buf_size] = c;
    q->data_size++;

    /* 若队列溢出, 最早的数据将被覆盖 */
    if (q->data_size > q->buf_size)
        queue_get(q);
}

/* 取数据, 需先判断队列非空 */
u8 queue_get(queue_st *q)
{
    u8 ret;

    ret = q->buf[q->data_index++];
    if (q->data_index >= q->buf_size)
        q->data_index = 0;
    q->data_size--;
    return ret;
}

/* 取指定位置数据, 不改变数据长度及数据起始位置
 * @offset: 相对数据起始位置的偏移, 不超过当前数据长度
 */
u8 queue_get_index(queue_st *q, u8 offset)
{
    return q->buf[(q->data_index + offset) % q->buf_size];
}

/* 标记指定长度的数据已取出 */
void queue_mark_get(queue_st *q, u8 mark_len)
{
    if (mark_len > q->data_size)
        mark_len = q->data_size;
    q->data_index += mark_len;
    if (q->data_index >= q->buf_size)
        q->data_index = 0;
    q->data_size -= mark_len;
}
