/* ***********************************************************************

* Name: link.h
* Desc: C语言ADT(抽象数据类型)的实现
* 本结构为线性表, 以链式存储方式组织
* 实现的操作有:
* 结构初始化/销毁与空间释放/向头部或尾部加入记录/在指定位置加入记录
* /在有序表中根据给定条件寻找合适位置加入记录/删除指定位置的记录
* /删除符合条件的记录/读取指定位置记录/查找符合条件的记录
* /按给定的条件排序(可自定义排序依据的字段及排序方向)
* Author: Joshua Chan
* Date: 2011-11-03
* ***********************************************************************/
#ifndef _LINK_H_
#define _LINK_H_

#include <stdbool.h>

/* 数据类型, 用户不可见 */
struct node_st {
void *datap;
struct node_st *prev, *next;
};

struct list_st {
struct node_st head;
int elmsize;
int elmnr;
};

/* 接口声明 */
typedef void proc_func_t(void*);
typedef int comp_func_t(void*, void*);

typedef void* LIST_T;

LIST_T list_init(const int elmsize);
int list_release(LIST_T ptr);
int list_putif(LIST_T ptr, comp_func_t comp, void *datap, const bool ord);
int list_putpos(LIST_T ptr, void *datap, const int pos);
int list_append(LIST_T ptr, void *datap);
int list_prepend(LIST_T ptr, void *datap);
int list_delete_pos(LIST_T ptr, const int pos);
int list_delete(LIST_T ptr, comp_func_t comp, void *key);
int list_travel(LIST_T ptr, proc_func_t proc);
void *list_getpos(LIST_T ptr, const int pos);
void *list_find(LIST_T ptr, comp_func_t comp, void *key);
int list_sort(LIST_T ptr, comp_func_t comp, const bool ord);

#endif