/* Name: test.c
 * Desc: C语言ADT(抽象数据类型)的实现测试程序
 * Author: Joshua Chan
 * Date: 2011-11-03
 */
#include <stdio.h>
#include <string.h>
#include "link.h"

/* 用户自定义结构                        */
struct info_st {
    int id;
    char name[20];
    int age;
};

/* 用户自定义比较函数                    */
int comp_id(void *data1, void *data2)
{
    struct info_st *a = data1;
    struct info_st *b = data2;

    if (a->id == b->id)
        return 0;
    return (a->id > b->id) ? 1 : -1;
}

/* 用户自定义比较函数                    */
int comp_name(void *data1, void *data2)
{
    struct info_st *a = data1;
    struct info_st *b = data2;

    return strcmp(a->name, b->name);
}

/* 用户自定义比较函数                    */
int comp_age(void *data1, void *data2)
{
    struct info_st *a = data1;
    struct info_st *b = data2;

    if (a->age == b->age)
        return 0;
    return (a->age > b->age) ? 1 : -1;
}

/* 用户自定义操作函数                    */
void proc_print(void *data)
{
    struct info_st *a = data;

    printf("%d\t%s\t%d\n", a->id, a->name, a->age);
}

void print_title(void)
{
    printf("====================\nID\tName\tAge\n");
}

struct info_st put_data(int id, char *name, int age)
{
    struct info_st new;

    new.id = id;
    strcpy(new.name, name);
    new.age = age;

    return new;
}

/* 测试程序                                */
int main(void)
{
    LIST_T l;
    struct info_st i;

    l = list_init(sizeof(struct info_st));

    /* 追加5条记录            */
    i = put_data(3, "Zhao", 20);
    list_append(l, &i);
    i = put_data(5, "Qian", 43);
    list_append(l, &i);
    i = put_data(7, "Sun", 3);
    list_append(l, &i);
    i = put_data(9, "Li", 14);
    list_append(l, &i);
    i = put_data(11, "Zhou", 28);
    list_append(l, &i);

    /* 遍历结构, 同时调用给定的函数        */
    printf("Add 5 records.\n");
    print_title();
    list_travel(l, proc_print);

    /* 在前端加入1条记录, 并查看效果    */
    i = put_data(4, "Wu", 59);
    list_prepend(l, &i);
    printf("\nAdd a record on head.\n");
    print_title();
    list_travel(l, proc_print);

    /* 在指定位置加入1条记录            */
    i = put_data(2, "Zheng", 33);
    list_putpos(l, &i, 2);
    printf("\nAdd a record on pos 2.\n");
    print_title();
    list_travel(l, proc_print);

    /* 取出指定位置的记录, 并显示        */
    i = *(struct info_st *)list_getpos(l, 3);
    printf("\nGet record on pos 3.\n");
    print_title();
    proc_print(&i);

    /* 查找符合条件的记录, 并显示        */
    i = put_data(0, "Zhao", 0);
    i = *(struct info_st *)list_find(l, comp_name, &i);
    printf("\nFind record which named Zhao.\n");
    print_title();
    proc_print(&i);

    /* 按name升序排序                    */
    list_sort(l, comp_name, true);
    printf("\nSorted by name with ascending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 按age降序排序                    */
    list_sort(l, comp_age, false);
    printf("\nSorted by age with descending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 自动按age顺序将记录加入到合适的位置    */
    i = put_data(1, "Wang", 30);
    list_putif(l, comp_age, &i, false);
    printf("\nAuto put the new record on the proper pos.\n");
    print_title();
    list_travel(l, proc_print);

    /* 按id升序排序                        */
    list_sort(l, comp_id, true);
    printf("\nSorted by id with ascending order.\n");
    print_title();
    list_travel(l, proc_print);

    /* 删除指定位置记录                    */
    list_delete_pos(l, 3);
    printf("\nDelete record on pos 3.\n");
    print_title();
    list_travel(l, proc_print);

    /* 删除符合条件的记录                */
    i = put_data(0, "Li", 0);
    list_delete(l, comp_name, &i);
    printf("\nDelete record which named Li.\n");
    print_title();
    list_travel(l, proc_print);

    /* 结构销毁, 释放空间                */
    list_release(l);

    return 0;
}