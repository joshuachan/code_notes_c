/* ***************************************************
 * Name: array.h
 * Desc: 建立顺序存储线性表结构, 并实现相应的操作. 包括:
 *       初始化/销毁/追加/查找/删除/查看/排序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#ifndef _ARRAY_H_
#define _ARRAY_H_

#include <stdbool.h>

#define STEP    3       /* 预分配元素空间步长        */

typedef int data_t;     /* 数据类型声明              */

struct array {
    data_t *data;       /* 数据存储区域指针          */
    int size;           /* 数据类型长度              */
    int num;            /* 已有数据个数              */
    int load;           /* 当前能容纳最大数据量      */
};

int array_init(struct array *l);
void array_release(struct array *l);
int array_append(struct array *l, data_t *data);
int array_find(struct array *l, int cmp(data_t *, void *), void *key);
int array_delete(struct array *l, int pos);
void array_print(struct array *l);
void array_sort(struct array *l, bool order);

#endif