/* ***************************************************
 * Name: test.c
 * Desc: 顺序存储线性表操作函数测试程序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "array.h"

/* 查找比较函数                    */
int cmp(data_t *data, void *key)
{
    if (*data == *(data_t *)key)
        return 0;
    else if (*data > *(data_t *)key)
        return 1;
    else
        return -1;
}

void put_data(data_t *data, int n)
{
    *data = n;
}

int main(void)
{
    int i, key, find;
    struct array l;
    data_t data;

    /* 数组初始化                            */
    array_init(&l);
    srand(time(NULL));
    /* 以小于10的随机数填充数组                */
    for (i = 1; i < 9; i++) {
        put_data(&data, rand()%10);
        array_append(&l, &data);
    }
    array_print(&l);

    key = 3;
    /* 查找并删除含有指定数据的数组元素        */
    if ((find = array_find(&l, &cmp, &key)) < 0)
        printf("%d was not found\n", key);
    else {
        array_delete(&l, find);
        printf("%d was found in pos=%d and deleted\n", key, find);
        array_print(&l);
    }

    /* 数组排序                                */
    array_sort(&l, true);
    printf("data was sorted\n");
    array_print(&l);

    array_release(&l);
    return 0;
}