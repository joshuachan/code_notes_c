/* ***************************************************
 * Name: array.c
 * Desc: 建立顺序存储线性表结构, 并实现相应的操作. 包括:
 *       初始化/销毁/追加/查找/删除/查看/排序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "array.h"

/* 结构初始化                */
int array_init(struct array *l)
{
    l->size = sizeof(data_t);
    l->num = 0;
    l->load = STEP;
    l->data = (data_t *)calloc(l->load, l->size);
    return (l->data == NULL) ? -1 : 0;
}

/* 销毁, 空间释放                    */
void array_release(struct array *l)
{
    if (l->data != NULL) {
        free(l->data);
        l->data = NULL;
    }
}

/* 追加数据                            */
int array_append(struct array *l, data_t *data)
{
    if (l->load <= l->num) {
        l->load += STEP;
        l->data = (data_t *)realloc(l->data, l->load * l->size);
        if (l->data == NULL)
            return -1;
    }
    l->data[l->num] = *data;
    l->num++;

    return 0;
}

/* 根据指定条件查找数据在数组中的位置, 以下标表示    */
int array_find(struct array *l, int cmp(data_t *, void *), void *key)
{
    int i;

    for (i = 0; i < l->num; i++)
        if (cmp(&l->data[i], key) == 0)
            return i;

    return -1;
}

/* 删除数组中指定下标处的数据, 并将其后数据向前移动        */
int array_delete(struct array *l, int pos)
{
    int i;

    if (pos < 0 || pos > l->num - 1)
        return -1;
    for (i = pos; i < l->num - 1; i++)
       l->data[i] = l->data[i+1];
    l->num--;
    return 0;
}

/* 遍历打印数据            */
void array_print(struct array *l)
{
    int i;

    for (i = 0; i < l->num; i++)
        printf("%d ", l->data[i]);
    putchar('\n');
}

static inline void swap(data_t *a, data_t *b)
{
    data_t tmp;

    tmp = *a;
    *a = *b;
    *b = tmp;
}

/* 起泡法排序
 * @order: true为升序, false为降序
 */
void array_sort(struct array *l, bool order)
{
    int i, j;
    bool flag;

    for (i = 0; i < l->num; i++) {
        flag = true;
        for (j = 0; j < l->num - i - 1; j++) {
            switch (order) {
            case true:
                if (l->data[j] > l->data[j+1]) {
                    swap(&(l->data[j]), &(l->data[j+1]));
                    flag = false;
                }
                break;
            case false:
                if (l->data[j] < l->data[j+1]) {
                    swap(&(l->data[j]), &(l->data[j+1]));
                    flag = false;
                }
                break;
            }
        }
        if (flag == true)
            break;
    }
}