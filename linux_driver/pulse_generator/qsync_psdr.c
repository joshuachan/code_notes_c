/* *******************************************************************
 * File Name: qsync_psdr.c
 * Function:  驱动S3C6410的GPQ4,5,6端口(对应偏高,偏低,合闸)产生脉冲
 * Description: 1. 通过ioctl方法完成以下工作:
 *                 设置脉冲的宽度, 周期, 类型, 开始发送, 停止发送, 读脉宽及周期.
 *              2. 脉冲宽度及周期未设置时, 默认值为0, 当脉冲周期小于脉宽时, 脉冲
 *                 只发送一次, 否则持续发送.
 *              3. 接收到停止发送的命令后, 当前脉冲会立即停止.
 *              4. 接收到开始发送的命令后, 新脉冲会立即启动.
 * Author & Date: Joshua Chan, 2011/12/26
 * ******************************************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/jiffies.h>
#include <linux/time.h>
#include <linux/timer.h>
#include <linux/uaccess.h>
#include <asm/io.h>
#include <mach/map.h>
#include <mach/regs-gpio.h>
#include <mach/gpio-bank-q.h>
#include "qsync_psdr.h"

/* 配置端口为output状态 */
static void config_port_output(int gpq_port)
{
    unsigned long gpqcon;

    gpqcon = __raw_readl(S3C64XX_GPQCON);
    gpqcon &= ~(0x03 << (gpq_port * 2));
    gpqcon |= (0x01 << (gpq_port * 2));
    __raw_writel(gpqcon, S3C64XX_GPQCON);
}

/* 设置端口输出电平 */
static inline void set_port_high(unsigned long gpq_port)
{
    unsigned long gpqdat;

    gpqdat = __raw_readl(S3C64XX_GPQDAT);
    gpqdat |= (0x01 << gpq_port);
    __raw_writel(gpqdat, S3C64XX_GPQDAT);
}

/* 设置端口输出电平 */
static inline void set_port_low(unsigned long gpq_port)
{
    unsigned long gpqdat;

    gpqdat = __raw_readl(S3C64XX_GPQDAT);
    gpqdat &= ~(0x01 << gpq_port);
    __raw_writel(gpqdat, S3C64XX_GPQDAT);
}

/* 设置端口输出电平 */
static inline void set_port_456_low(void)
{
    unsigned long gpqdat;

    gpqdat = __raw_readl(S3C64XX_GPQDAT);
    gpqdat &= ~(0x07 << 4);
    __raw_writel(gpqdat, S3C64XX_GPQDAT);
}

static struct timer_list timer_high;  //高电平定时器
static struct timer_list timer_cycle; //周期定时器
static volatile unsigned long time_fm_high;  //调频脉冲高电平时间
static volatile unsigned long time_fm_cycle; //调频脉冲周期
static volatile unsigned long time_cs_high;  //合闸脉冲高电平时间
static volatile unsigned long time_cs_cycle; //合闸脉冲周期
static volatile unsigned long work_port;     //脉冲发送端口

static inline int is_fmport(void)
{
    return (work_port == HIGHER_PULSE_PORT || work_port == LOWER_PULSE_PORT)
        ? 1 : 0;
}

static inline int is_csport(void)
{
    return (work_port == CLOSING_PULSE_PORT) ? 1 : 0;
}

/* 高电平输出定时器函数 */
static inline void timer_high_function(unsigned long empty)
{
    set_port_456_low();
    //printk("time: %u, port high level end\n", jiffies_to_msecs(jiffies));
}

static inline void timer_start(void);

/* 低电平输出定时器函数 */
static inline void timer_cycle_function(unsigned long empty)
{
    //printk("time: %u, one cycle end\n", jiffies_to_msecs(jiffies));
    if ((is_fmport() && time_fm_high < time_fm_cycle)
        || (is_csport() && time_cs_high < time_cs_cycle))
        timer_start();
}

static inline void timer_high_add(void)
{
    if (is_fmport())
        timer_high.expires = jiffies + msecs_to_jiffies(time_fm_high);
    else if (is_csport())
        timer_high.expires = jiffies +  msecs_to_jiffies(time_cs_high);
    timer_high.function = timer_high_function;
    add_timer(&timer_high);
}

static inline void timer_cycle_add(void)
{
    if (is_fmport())
        timer_cycle.expires = jiffies + msecs_to_jiffies(time_fm_cycle);
    else if (is_csport())
        timer_cycle.expires = jiffies + msecs_to_jiffies(time_cs_cycle);
    timer_cycle.function = timer_cycle_function;
    add_timer(&timer_cycle);
}

static inline void timer_start(void)
{
    //printk("time: %u, port %lu, high level start\n",
    //jiffies_to_msecs(jiffies), work_port);

    if ((is_fmport() && time_fm_high > 0)
        || (is_csport() && time_cs_high > 0)) {
        set_port_high(work_port);
        timer_high_add();
    }

    if (is_fmport() || is_csport())
        timer_cycle_add();
}

/* 停止定时器函数 */
static inline void timer_stop(void)
{
    //printk("time: %u, stop command received\n", jiffies_to_msecs(jiffies));

    set_port_456_low();
    if (timer_pending(&timer_high))
        del_timer(&timer_high);
    if (timer_pending(&timer_cycle))
        del_timer(&timer_cycle);
}

/* 设备打开时, 配置端口为output状态, 并初始化定时器 */
static int pulse_gen_open(struct inode *inode, struct file *file)
{
    //printk("port opened\n");

    config_port_output(HIGHER_PULSE_PORT);
    config_port_output(LOWER_PULSE_PORT);
    config_port_output(CLOSING_PULSE_PORT);

    set_port_low(HIGHER_PULSE_PORT);
    set_port_low(LOWER_PULSE_PORT);
    set_port_low(CLOSING_PULSE_PORT);

    init_timer(&timer_high);
    init_timer(&timer_cycle);

    return 0;
}

/* 关闭设备时, 配置端口为reserved状态 */
static int pulse_gen_close(struct inode *inode, struct file *file)
{
    //printk("port closed\n");
    timer_stop();
    return 0;
}

/* 定义ioctl方法 */
static long pulse_gen_ioctl(struct file *file, unsigned int cmd,
                           unsigned long arg)
{
    //printk("ioctl cmd received.\n");
    int ret = 0;

    /* 检测命令有效性 */
    if (_IOC_TYPE(cmd) != PS_IOC_MAGIC)
        return -EINVAL;

    switch (cmd) {
        /************** 调频脉冲ioctl命令 ****************/
    case PS_FM_TYPE_SET:  //设置脉冲类型
        if (arg == FM_TYPE_HIGHER)
            work_port = HIGHER_PULSE_PORT;
        else if (arg == FM_TYPE_LOWER)
            work_port = LOWER_PULSE_PORT;
        else
            ret = -EINVAL;
        break;
    case PS_FM_TYPE_GET:  //读脉冲类型
        if (work_port == HIGHER_PULSE_PORT)
            ret = __put_user(FM_TYPE_HIGHER, (unsigned long *)arg);
        else if (work_port == LOWER_PULSE_PORT)
            ret = __put_user(FM_TYPE_LOWER, (unsigned long *)arg);
        else
            ret = -EINVAL;
        break;
    case PS_FM_WIDTH_SET: //设置脉宽
        time_fm_high = arg;
        break;
    case PS_FM_WIDTH_GET: //读脉宽
        ret = __put_user(time_fm_high, (unsigned long *)arg);
        break;
    case PS_FM_CYCLE_SET: //设置脉冲周期
        time_fm_cycle = arg;
        break;
    case PS_FM_CYCLE_GET: //读脉冲周期
        ret = __put_user(time_fm_cycle, (unsigned long *)arg);
        break;
    case PS_FM_START:     //开始发送
        timer_stop();
        timer_start();
        break;
    case PS_FM_STOP:      //停止发送
        timer_stop();
        break;

        /************** 合闸脉冲ioctl命令 ****************/
    case PS_CS_WIDTH_SET: //设置脉宽
        time_cs_high = arg;
        break;
    case PS_CS_WIDTH_GET: //读脉宽
        ret = __put_user(time_cs_high, (unsigned long *)arg);
        break;
    case PS_CS_CYCLE_SET: //设置脉冲周期
        time_cs_cycle = arg;
        break;
    case PS_CS_CYCLE_GET: //读脉冲周期
        ret = __put_user(time_cs_cycle, (unsigned long *)arg);
        break;
    case PS_CS_START:     //开始发送
        work_port = CLOSING_PULSE_PORT;
        timer_stop();
        timer_start();
        break;
    case PS_CS_STOP:      //停止发送
        timer_stop();
        break;

    default:
        ret = -EINVAL;
        break;
    }

    return ret;
}

/* 构建file_operations结构 */
static struct file_operations pulse_gen_fops = {
    .owner = THIS_MODULE,
    .open = pulse_gen_open,
    .release = pulse_gen_close,
    .unlocked_ioctl = pulse_gen_ioctl,
};

/* 构建混杂设备结构 */
static struct miscdevice pulse_gen_misc = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = PULSE_DEVICE_NAME,
    .fops = &pulse_gen_fops,
};

/* 驱动入口函数 */
static int __init pulse_gen_init(void)
{
    //printk("port init\n");

    config_port_output(HIGHER_PULSE_PORT);
    config_port_output(LOWER_PULSE_PORT);
    config_port_output(CLOSING_PULSE_PORT);

    set_port_low(HIGHER_PULSE_PORT);
    set_port_low(LOWER_PULSE_PORT);
    set_port_low(CLOSING_PULSE_PORT);

    misc_register(&pulse_gen_misc);
    return 0;
}

/* 驱动出口函数 */
static void __exit pulse_gen_exit(void)
{
    //printk("port exit\n");
    misc_deregister(&pulse_gen_misc);
}

module_init(pulse_gen_init);
module_exit(pulse_gen_exit);
MODULE_LICENSE("GPL");