/* *************************************************************
 * File name:   eeprom_test.c
 * Function:    eeprom��д����
 * Description: eeprom��д�������Գ���
 * Author & Date: Joshua Chan, 2011/12/18 
 * *************************************************************/
#include "eeprom_io.h"

int main(int argc, char *argv[])
{
    u8 pos = 3;
    u8 data[5] = {32, 15, 99, 250, 6};

    eeprom_byte_write(pos, data[0]);  //���ֽ�д����

    data[0] = eeprom_byte_read(pos);  //���ֽڶ�����

    eeprom_page_write(pos, data, 5);  //���ֽ�д����

    eeprom_page_read(pos, data, 5);   //���ֽڶ�����

    return 0;
}