#define    GLOBAL_CLK        1
#include <stdlib.h>
#include <string.h>
#include "def.h"
#include "option.h"
#include "2440addr.h"
#include "2440lib.h"
#include "2440slib.h"
#include "mmu.h"
#include "profile.h"
#include "memtest.h"
#include "main.h"
#include "adj_sc.h"

volatile U32 preScaler;
int xdata=0, ydata=0;

static void __irq AdcTsAuto(void);
void Test_Touchpanel(void);
static void Lcd_EnvidOnOff(int onoff);
static void Lcd_Port_Init( void );
static void LCD_Init(void);
void Set_Clk(void);
static void cal_cpu_bus_clk(void);

int Main(void)
{   
  
    MMU_Init();
    Set_Clk();

    LCD_Init();

    Test_Touchpanel();

    while(1);
    return 0;

}   

void Test_Touchpanel(void)
{
    rADCDLY=50000;            //Normal conversion mode delay about (1/3.6864M)*50000=13.56ms
   
    /*设置AD转频率*/
    preScaler = ADC_FREQ;
    preScaler = 50000000/ADC_FREQ - 1; //PCLK=50M
    rADCCON = (1<<14)|(preScaler<<6);    //ADCPRS En,PRSCVL
    // rADCCON=(1<<14)+(preScaler<<6);   //ADCPRS En, ADCPRS Value
   
    /* 开始显示触屏校准提示信息, 并提示第1个触点位置 */
    draw_char(72, 6, F_COLOR, B_COLOR, "Adjusting Touch Screen", 0);   
    scroll_display("Please touch the 1st point.");   
    draw_cross(40, 30, 15, F_COLOR);

    /*设置触摸屏为等待中断模式，等待触摸笔被按下*/
    rADCTSC=0xd3;  //Wfait,XP_PU,XP_Dis,XM_Dis,YP_Dis,YM_En
   
    /*clear irq*/
    //ClearPending(BIT_ADC);
    rSRCPND = 0x80000000;
    rINTPND = 0x80000000;
    ClearSubPending(BIT_SUB_TC);   

    pISR_ADC = (U32)AdcTsAuto;

    /*enable INT_TC irq*/
    //EnableIrq(BIT_ADC);
    rINTMSK = 0x7fffffff;       
    EnableSubIrq(BIT_SUB_TC);   
}

static void __irq AdcTsAuto(void)
{   
    U32     saveAdcdly;

    // int s_len = 0;
    // int x_p = 0;
   
    /****************stylus down************************/   
    /*pull-up disable,自动连续X,Y坐标转换*/
    rADCTSC = (1<<3)|(1<<2);
    saveAdcdly=rADCDLY;
    rADCDLY=40000;            //Normal conversion mode delay about (1/50M)*40000=0.8ms
   
    /*开始AD转换*/
    rADCCON|=0x1;            //start ADC
    while(rADCCON & 0x1);        //check if Enable_start is low
   
    while(!(rADCCON & 0x8000));    //check if EC(End of Conversion) flag is high, This line is necessary~!!
   
    while(!(rSRCPND & 0x80000000));    //check if ADC is finished with interrupt bit

    /*获取X,Y坐标*/
    xdata=(rADCDAT0&0x3ff);
    ydata=(rADCDAT1&0x3ff);

    /* 调用触屏按下处理函数, 根据当前状态作出相应处理 */
    check_touch_down();
   
    ClearSubPending(BIT_SUB_TC);
    //ClearPending(BIT_ADC);
    rSRCPND = 0x80000000;
    rINTPND = 0x80000000; 
   

    EnableSubIrq(BIT_SUB_TC);
       //EnableIrq(BIT_ADC);
    rINTMSK = 0x7fffffff;
    /****************stylus down************************/   
   
    /****************stylus up**************************/
    /*设置触摸屏为等待中断模式，等待触摸笔抬起*/
    rADCTSC =0xd3;    //Waiting for interrupt
    rADCTSC=rADCTSC|(1<<8); // Detect stylus up interrupt signal.
   
    while(1)        //to check Pen-up state
    {
        if(rSUBSRCPND & (BIT_SUB_TC))    //check if ADC is finished with interrupt bit
        {
            break;    //if Stylus is up(1) state
        }
    }   
    /****************stylus up**************************/
    /* 调用触屏弹起处理函数, 根据当前状态作出相应处理 */
    check_touch_up();
   
    rADCDLY=saveAdcdly;
    /*设置触摸屏为等待中断模式，等待下次触摸笔按下*/
    rADCTSC =0xd3;    //Waiting for interrupt
   
    ClearSubPending(BIT_SUB_TC); 
    //ClearPending(BIT_ADC);
    rSRCPND = 0x80000000;
    rINTPND = 0x80000000;

    EnableSubIrq(BIT_SUB_TC);
       //EnableIrq(BIT_ADC);
    rINTMSK = 0x7fffffff;
}

/*LCD开关*/
static void Lcd_EnvidOnOff(int onoff)
{
    if(onoff==1)
    rLCDCON1|=1; // ENVID=ON
    else
    rLCDCON1 =rLCDCON1 & 0x3fffe; // ENVID Off
}

static void Lcd_Port_Init( void )
{
    rGPCUP=0xffffffff; // Disable Pull-up register
    rGPCCON=0xaaaa02a8; //Initialize VD[7:0],VM,VFRAME,VLINE,VCLK

    rGPDUP=0xffffffff; // Disable Pull-up register
    rGPDCON=0xaaaaaaaa; //Initialize VD[15:8]
}

/*LCD初始化*/
static void LCD_Init(void)
{
    Lcd_Port_Init();

    /*显示模式初始化*/
    /*bit[17:8](4:CLKVAL);bit[6:5](11:TFT LCD panel);bit[4:1](1100:16 bpp for TFT)*/
    rLCDCON1 = (LCD_PIXCLOCK << 8) | (3 <<  5) | (12 << 1);
   
    /*bit[31:24](1:VBPD);bit[23:14](320-1:行数);bit[13:6](5:VFPD);bit[5:0](1:VSPW)*/
       rLCDCON2 = (LCD_UPPER_MARGIN << 24) | ((LCD_HEIGHT - 1) << 14) | (LCD_LOWER_MARGIN << 6) | (LCD_VSYNC_LEN << 0);
      
       /*bit[25:19](36:HBPD);bit[18:8](240-1:列数);bit[7:0](19:HFPD)*/
       rLCDCON3 = (LCD_RIGHT_MARGIN << 19) | ((LCD_WIDTH  - 1) <<  8) | (LCD_LEFT_MARGIN << 0);
      
       /*bit[15:8](13:MVAL,只有当LCDCON1 bit[7]MMODE=1才有效);bit[7:0](5:HSPW)*/
       rLCDCON4 = (13 <<  8) | (LCD_HSYNC_LEN << 0);
      
       /*bit[11](5:6:5 Format);bit[9](VLINE/HSYNC polarity inverted);bit[8](VFRAME/VSYNC inverted)
      bit[3](Enalbe PWERN signal),bit[1](half-word swap control bit)*/
    rLCDCON5   =  (1<<11) | (1 << 9) | (1 << 8) | (1 << 3) | (1 << 0);   

    /*
    * 帧缓冲地址初始化
    * LCDBANK: 视频帧缓冲区内存地址30-22位
    * LCDBASEU: 视频帧缓冲区的开始地址21-1位
    * LCDBASEL: 视频帧缓冲区的结束地址21-1位    *
    * bit[29:21]:LCDBANK,bit[20:0]:LCDBASEU
    */
    rLCDSADDR1 = ((LCD_ADDR >> 22) << 21) | ((M5D(LCD_ADDR >> 1)) <<  0);
   
    /*bit[20:0]:LCDBASEL*/
    rLCDSADDR2 = M5D((LCD_ADDR + LCD_WIDTH * LCD_HEIGHT * 2) >> 1);   
   
    /*
     * PAGEWIDTH:虚拟屏幕一行的字节数，如果不使用虚拟屏幕，设置为实际屏幕的行字节数
     * OFFSIZE:虚拟屏幕左侧偏移的字节数，如果不使用虚拟屏幕，设置为0
     * bit[21:11]:OFFSIZE; bit[10:0]:PAGEWIDTH
     */
    rLCDSADDR3 = LCD_WIDTH;       

    /*屏蔽中断*/
    rLCDINTMSK |= 3;
      rTCONSEL   &= (~7);
 
    /*disable调色板*/
       rTPAL = 0x0;
      
       /*禁止LPC3600/LCC3600模式*/
       rTCONSEL &= ~((1<<4) | 1);
      
       /*打开LCD*/
       Lcd_EnvidOnOff(1);    
}

/*************************************************
Function name: Set_Clk()
Parameter    : void
Description     : 设置CPU的时钟频率
Return         : void
Argument     : void
Autor & date : Daniel
**************************************************/
void Set_Clk(void)
{
    int i;
    U8 key;
    U32 mpll_val = 0 ;
    i = 2 ;                 //don't use 100M!
                         //boot_params.cpu_clk.val = 3;
    switch ( i ) {
    case 0:    //200
        key = 12;
        mpll_val = (92<<12)|(4<<4)|(1);
        break;
    case 1:    //300
        key = 13;
        mpll_val = (67<<12)|(1<<4)|(1);
        break;
    case 2:    //400
        key = 14;
        mpll_val = (92<<12)|(1<<4)|(1);
        break;
    case 3:    //440!!!
        key = 14;
        mpll_val = (102<<12)|(1<<4)|(1);
        break;
    default:
        key = 14;
        mpll_val = (92<<12)|(1<<4)|(1);
        break;
    }
   
    //init FCLK=400M, so change MPLL first
    ChangeMPllValue((mpll_val>>12)&0xff, (mpll_val>>4)&0x3f, mpll_val&3);   //set the register--rMPLLCON
    ChangeClockDivider(key, 12);    //the result of rCLKDIVN [0:1:0:1] 3-0 bit
    cal_cpu_bus_clk();    //HCLK=100M   PCLK=50M
}

/*************************************************
Function name: cal_cpu_bus_clk
Parameter    : void
Description     : 设置PCLK\HCLK\FCLK的频率
Return         : void
Argument     : void
Autor & date : Daniel
**************************************************/
static void cal_cpu_bus_clk(void)
{
    static U32 cpu_freq;
    static U32 UPLL;
   
    U32 val;
    U8 m, p, s;
   
    val = rMPLLCON;
    m = (val>>12)&0xff;
    p = (val>>4)&0x3f;
    s = val&3;

    //(m+8)*FIN*2 不要超出32位数!
    FCLK = ((m+8)*(FIN/100)*2)/((p+2)*(1<<s))*100;     //FCLK=400M  FIN=12000000
   
    val = rCLKDIVN;
    m = (val>>1)&3;
    p = val&1;   
    val = rCAMDIVN;
    s = val>>8;
   
    switch (m) {
    case 0:
        HCLK = FCLK;
        break;
    case 1:
        HCLK = FCLK>>1;
        break;
    case 2:
        if(s&2)
            HCLK = FCLK>>3;
        else
            HCLK = FCLK>>2;
        break;
    case 3:
        if(s&1)
            HCLK = FCLK/6;
        else
            HCLK = FCLK/3;
        break;
    }
   
    if(p)
        PCLK = HCLK>>1;
    else
        PCLK = HCLK;
   
    if(s&0x10)
        cpu_freq = HCLK;
    else
        cpu_freq = FCLK;
       
    val = rUPLLCON;
    m = (val>>12)&0xff;
    p = (val>>4)&0x3f;
    s = val&3;
    UPLL = ((m+8)*FIN)/((p+2)*(1<<s));
    UCLK = (rCLKDIVN&8)?(UPLL>>1):UPLL;
}