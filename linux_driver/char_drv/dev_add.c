/* ***************************************
 * Name: drv_add.c
 * Desc: Linux简单字符设备驱动编写方法
 * Author & Date: Joshua Chan, 2011/11/25
 * **************************************/
#include <linux/device.h>
#include <linux/module.h>
#include <linux/fs.h>

int major;               // 主设备号, 可在/proc/devices中查看
int minor = 0;
char device_name[] = "mydevice";
char class_name[] = "myclass";
char char_name[] = "mychar";

struct class *myclass;   // 定义字符设备类, 可在/sys/class/中查看
struct device *mydevice; // 定义字符设备, 设备文件位于/dev/目录

/* 为file_operations结构体准备open函数 */
int myopen(struct inode *inode, struct file *file)
{
    printk("My device opened.\n");
    return 0;
}

/* 为file_operations结构体准备write函数 */
ssize_t mywrite(struct file *file, const char __user *buf, size_t size, loff_t *l)
{
    printk("My device wrote.\n");
    return 0;
}

/* 为register_chrdev()作准备, 定义file_operations结构体, 填充操作函数 */
struct file_operations myfops = {
    .owner = THIS_MODULE,
    .open = myopen,
    .write = mywrite,
};

/* 字符设备初始化函数 */
int __init myinit(void)
{
    /* 为将设备操作函数写入内核, 需调用register_chrdev()函数
       将第一个参数主设备号置0, 系统将自动分配并返回设备号 */
    major = register_chrdev(0, char_name, &myfops);

    /* 为让系统自动生成设备文件, 先调用class_create()函数注册设备类 */
    myclass = class_create(THIS_MODULE, class_name);

    /* 调用device_create(), 系统将根据设备类和主次设备号生成结点文件
       注：有的Linux版本中使用class_device_creat()生成设备文件 */
    mydevice = device_create(myclass, NULL, MKDEV(major, minor), NULL, device_name);

    return 0;
}

void __exit myexit(void)
{
    /* 调用unregister_chrdev()删除设备驱动 */
    unregister_chrdev(major, char_name);

    /* 调用device_unregister()函数删除设备文件 */
    device_unregister(mydevice);

    /* 调用class_destroy()函数删除设备类 */
    class_destroy(myclass);
}

module_init(myinit);
module_exit(myexit);

MODULE_LICENSE("GPL");