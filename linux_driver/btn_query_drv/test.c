/* ***************************************
 * Name: btn_test.c
 * Proj: 简单按键驱动程序查询方式实现(Mini2440)
 * Desc: 驱动测试程序
 * Auth & Date: Joshua Chan 2011/11/28
 * **************************************/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>

int main(void)
{
    int fd, i;
    int count = 0;
    unsigned char s[6];

    /* 打开设备文件 */
    fd = open("/dev/my_buttons", O_RDONLY);
    if (fd < 0) {
        printf("Open buttons error.\n");
        return -1;
    }

    /* 循环方式查询并打印按键信息 */
    while (true) {
        i = read(fd, s, 6);
        if (i != sizeof(s)) {
            printf("Read buttons error.\n");
            return -1;
        }
        printf("%d: ", count++);
        for (i = 0; i < 6; i++) {
            printf("%s", s[i] ? "1" : "0");
        }

        putchar('\n');

        for (i = 0; i < 6; i++) {
            if (s[i] == 0)
                printf("Button %d is pressed.\n", i + 1);
        }

        sleep(1);
    }

    close(fd);
    return 0;
}