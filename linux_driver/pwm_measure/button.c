/* ******************************************************
 * Name: button.c
 * Proj: 测量脉冲宽度(Mini2440)
 * Desc: 用按键模拟脉冲源, 计算从按键抬起至下次按键按下所经过的时间
         本模块通过按键动作, 使引脚电平发生变化, 从而触发中断, 并
         在中断处理函数中读取定时器的值, 实现脉冲时间的精确测量
 * Author & Date: Joshua Chan, 2011/12/02
 * ******************************************************/
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <mach/regs-gpio.h>
#include <mach/hardware.h>
#include <plat/regs-timer.h>
#include <asm/io.h>

/* Mini2440的按键key0, 对应端口为GPG0, 对应中断为EINT8 */

/* 为了使驱动中的read()函数在没有中断发生时休眠, 需先定义一个等待队列 */
static DECLARE_WAIT_QUEUE_HEAD(btn_wq);

/* 定义等待队列的唤醒条件 */
static volatile int btn_press;

/* 读取相应键值, 即GPGDAT相应位的值, 通过键值可以判断按下/抬起状态 */
static unsigned int key_val;

/* 由于这里使用内核函数s3c2410_gpio_getpin()读取GPGDAT的值,
   所以需要提供按键的pin值, 并作为request_irq()的参数传入 */
static unsigned int pin = S3C2410_GPG0;

/* 定义容纳2个元素的数组, 第1个位置存放定时器中断计数值, 第2个位置
   存放寄存器TCNTO已计数的值, 其中, t1为开始读数, t2为结束读数,
   t为计算结果 */
static unsigned long t1[2];
static unsigned long t2[2];
static unsigned long t[2];

/* 外部变量, 在定时器模块中有定义 */
extern unsigned long my_jif;
extern unsigned long tcnt;

/* 读取TCNTO的当前值 */
static inline unsigned short get_tcnto0(void)
{
    return __raw_readw(S3C2410_TCNTO(0));
}

/* 处理计数结果 */
static inline void calc(void)
{
    if (t2[1] >= t1[1]) {
        t[0] = t2[0] - t1[0];
        t[1] = t2[1] - t1[1];
    } else {
        t[0] = t2[0] - 1 - t1[0];
        t[1] = t2[1] + tcnt - t1[1];
    }
}

static int count;    // 测量次数计数
static int flag = 0; // 计数状态, 1表示计数已开始

/* 定义中断处理函数, 读取中断按键的状态值(按下为低, 抬起为高),
   得出计时结果, 并唤醒等待队列 */
static irqreturn_t btn_isr(int irq, void *pin)
{
    key_val = !s3c2410_gpio_getpin(*(unsigned int *)pin);
    if (key_val) {       // 按键按下
        if (flag == 1) { // 计数已开始
            flag = 0;    // 停止计数
            t2[0] = my_jif;
            t2[1] = tcnt - (unsigned long)get_tcnto0();
            calc();
            printk("Timer %d stopped.\n", count);
            printk("t[0] = %lu, t[1] = %lu\n", t[0], t[1]);

            /* 唤醒read()函数 */
            btn_press = 1;
            wake_up_interruptible(&btn_wq);
        }
    } else {      // 按键抬起
        flag = 1; // 计数开始
        t1[0] = my_jif;
        t1[1] = tcnt - (unsigned long)get_tcnto0();
        printk("Timer %d start...\n", ++count);
    }

    return IRQ_HANDLED;
}

/* 注册中断, 完成申请中断号, 向内核加入中断处理函数, 设定中断触发条件等任务,
   注册成功后可在/proc/interrupts中查看 */
static int btn_open(struct inode *inode, struct file *file)
{
    request_irq(IRQ_EINT8,  btn_isr, IRQ_TYPE_EDGE_BOTH, "my_button1", &pin);

    return 0;
}

/* 定义驱动中的read()函数, 将测量结果传回用户空间, 无中断发生则休眠 */
static ssize_t btn_read(struct file *file, char __user *buf,
                        size_t count, loff_t *loff)
{
    wait_event_interruptible(btn_wq, btn_press);
    btn_press = 0;
    copy_to_user(buf, &t, sizeof(t));

    return sizeof(t);
}

/* 驱动中的release()函数, 注销中断 */
static int btn_close(struct inode *inode, struct file *file)
{
    free_irq(IRQ_EINT8,  &pin);
    return 0;
}

/* 驱动中的设备操作方法结构体 */
static struct file_operations btn_fops = {
    .owner = THIS_MODULE,
    .open = btn_open,
    .read = btn_read,
    .release = btn_close,
};

/* 定义混杂设备结构 */
static struct miscdevice btn_misc = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "my_button",
    .fops = &btn_fops,
};

/* 驱动入口函数, 完成驱动注册, 设备自动生成工作 */
static __init int btn_init(void)
{
    misc_register(&btn_misc);
    return 0;
}

/* 驱动出口函数, 完成驱动清理工作 */
static __exit void btn_exit(void)
{
    misc_deregister(&btn_misc);
}

module_init(btn_init);
module_exit(btn_exit);
MODULE_LICENSE("GPL");