/* ************************************
 * Name: btn_int_test.c
 * Proj: 按键驱动测试程序
 * Auth & Date: Joshua Chan, 2011/11/29
 * ***********************************/
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
    int fd;
    unsigned int buf;

    fd = open("/dev/my_buttons", O_RDONLY);
    if (fd < 0) {
        printf("Open /dev/my_buttons failed.\n");
        return -1;
    }

    /* 循环读取产生中断的按键的键值并打印 */
    while (true) {
        read(fd, &buf, 4);
        printf("key value = 0x%x\n", buf);
    }

    close(fd);

    return 0;
}