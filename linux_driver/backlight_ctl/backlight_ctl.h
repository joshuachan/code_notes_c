/* ******************************************
 * File name: backlight_ctl.h
 * Function:  控制tiny6410背光亮度
 * Description: 应用驱动提供的接口控制背光的亮度
 * Author & Date: Joshua Chan, 2011/12/31
 * ******************************************/
#ifndef _BACKLIGHT_CTL_H
#define _BACKLIGHT_CTL_H

#define BACKLIGHT_PATH  "/dev/backlight-1wire"

/* 预设亮度值 */
#define BACKLIGHT_OFF    0   //关
#define BACKLIGHT_WEAK   32  //四分之一
#define BACKLIGHT_HALF   64  //二分之一
#define BACKLIGHT_BRIGHT 96  //四分之三
#define BACKLIGHT_FULL   127 //最大

/* 设置LCD背光亮度, 亮度取值范围0 ~ 127 */
extern int set_backlight_brightness(unsigned bright);

#endif