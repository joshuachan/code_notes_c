/* ******************************************
 * File name: test.c
 * Function:  ����tiny6410��������
 * Description: ������Ʋ��Գ���
 * Author & Date: Joshua Chan, 2011/12/31
 * ******************************************/
#include <stdio.h>
#include <stdlib.h>
#include "backlight_ctl.h"

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("usage: %s brightness(0 ~ 127)\n", argv[0]);
        return -1;
    }

    set_backlight_brightness((unsigned)atoi(argv[1]));

    return 0;
}