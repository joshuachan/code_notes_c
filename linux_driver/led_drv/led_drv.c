/* ***************************************
 * Name: led_drv.c
 * Desc: Mini2440 LED 驱动程序 for Linux
 * Author & Date: Joshua Chan, 2011/11/26
 * **************************************/
#include <linux/device.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/kdev_t.h>

/* 查询设备手册, 找到Mini2440的LED1~4对应GPB5~8, 找到GPB起始物理地址 */
#define GPB_BASE 0x56000010
static volatile unsigned long *gpbcon;  // 定义GPBCON虚拟地址
static volatile unsigned long *gpbdat;  // 定义GPBDAT虚拟地址
static int major;                       // 主设备号, 可在/proc/devices中查看
static struct class *led_class;         // 定义LED类, 可在/sys/class中查看
static struct device *led_device[5];    // 定义5个LED设备, 分别控制单个和全部LED

/* 为file_operations结构体作准备, 定义LED设备文件的write函数
   根据file结构找到次设备号, 写入数据<on/off>对应开和关 */
static ssize_t led_write(struct file *file, const char __user *buf,
                         size_t size, loff_t *off)
{
    /* 开辟buffer, 利用copy_from_user()传入用户空间写入的数据 */
    char s[16] = "";
    copy_from_user(s, buf, size);

    /* 找到次设备号, 向GPBDAT的对应位写入值 */
    if (!strcasecmp(s, "on")) {
        switch (MINOR(file->f_dentry->d_inode->i_rdev)) {
        case 0:
            *gpbdat &= ~((1 << 5) | (1 << 6) | (1 << 7) | (1 << 8));
            break;
        case 1:
            *gpbdat &= ~(1 << 5);
            break;
        case 2:
            *gpbdat &= ~(1 << 6);
            break;
        case 3:
            *gpbdat &= ~(1 << 7);
            break;
        case 4:
            *gpbdat &= ~(1 << 8);
            break;
        }
    } else if (!strcasecmp(s, "off")) {
        switch (MINOR(file->f_dentry->d_inode->i_rdev)) {
        case 0:
            *gpbdat |= (1 << 5) | (1 << 6) | (1 << 7) | (1 << 8);
            break;
        case 1:
            *gpbdat |= (1 << 5);
            break;
        case 2:
            *gpbdat |= (1 << 6);
            break;
        case 3:
            *gpbdat |= (1 << 7);
            break;
        case 4:
            *gpbdat |= (1 << 8);
            break;
        }
    }

    return 0;
}

/* 为register_chrdev()作准备, 定义file_operations结构体, 并填充操作方法 */
static struct file_operations led_fops = {
    .owner = THIS_MODULE,
    .write = led_write,
};

/* LED设备初始化函数 */
static int __init led_init(void)
{
    int i;

    /* 系统不能直接访问物理地址, 通过ioremap()将GPB物理地址映射为虚拟地址 */
    gpbcon = (volatile unsigned long *)ioremap(GPB_BASE, 16);

    /* 配置LED引脚为output状态 */
    *gpbcon &= ~((3 << 10) | (3 << 12) | (3 << 14) | (3 << 16));
    *gpbcon |= (1 << 10) | (1 << 12) | (1 << 14) | (1 << 16);

    /* 将LED全关 */
    gpbdat = gpbcon + 1;
    *gpbdat |= (1 << 5) | (1 << 6) | (1 << 7) | (1 << 8);

    /* 为了将设备文件的操作方法写入内核, 调用register_chrdev()函数
       将第一个参数主设备号置0, 系统将自动分配并返回主设备号 */
    major = register_chrdev(0, "led", &led_fops);
    if (major < 0) {
        printk("register_chrdev() failed.\n");
        iounmap(gpbcon);
        return -1;
    }

    /* 为了让系统自动生成设备文件, 调用class_create(), 注册设备类 */
    led_class = class_create(THIS_MODULE, "led_class");
    if (IS_ERR(led_class)) {
        printk("class_create() failed.\n");
        unregister_chrdev(major, "led");
        iounmap(gpbcon);
        return -1;
    }

    /* 调用device_create(), 根据设备类和主设备号生成LED设备文件 */
    led_device[0] = device_create(led_class, NULL, MKDEV(major, 0), NULL, "leds");
    for (i = 1; i < 5; i++) {
        led_device[i] = device_create(led_class, NULL, MKDEV(major, i), NULL, "led%d", i);
    }

    return 0;
}

/* LED设备卸载函数 */
static void __exit led_exit(void)
{
    int i;

    /* 调用device_unregister()删除设备文件 */
    for (i = 0; i < 5; i++)
        device_unregister(led_device[i]);

    /* 调用class_destroy()删除设备类 */
    class_destroy(led_class);

    /* 调用unregister_chrdev()删除设备驱动 */
    unregister_chrdev(major, "led");

    /* 调用iounmap()取消地址映射 */
    iounmap(gpbcon);
}

module_init(led_init);
module_exit(led_exit);

MODULE_LICENSE("GPL");