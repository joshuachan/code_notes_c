/* ***************************************
 * Name: led_test.c
 * Desc: Mini2440 LED驱动测试程序
 * Author & Date: Joshua Chan, 2011/11/26
 * **************************************/
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

/* 驱动测试程序
   其中: 设备文件led1~4操作单个led, 文件leds同时操作4个led */
int main(int argc, char *argv[])
{
    int fd;

    if (argc != 3) {
        printf("Usage:\n");
        printf("%s  <device_path>  <on|off>\n", argv[0]);
        return -1;
    }
    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
        printf("Open failed: %s\n", argv[1]);
        return -1;
    }
    write(fd, argv[2], strlen(argv[2]));
    return 0;
}