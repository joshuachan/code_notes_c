/* ******************************************************
 * File Name: uart.c
 * Function:  AVR UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能, 方便程序调试
 * Authon & Date: Joshua Chan, 2012/04/01, 2012/04/14更新
 * ******************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ioavr.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <avr_macros.h>
#include "uart.h"

/* UART初始化 */
void uart_init(u32 baud)
{
    u16 ubrr;

    /* 设置baudrate */
    ubrr = (u16)((FOSC / (8 * baud)) - 1);
    UBRR0H = (u8)(ubrr >> 8);
    UBRR0L = (u8)ubrr;
    /* 倍速发送 */
    UCSR0A |= 1 << U2X0;
    /* 设置帧格式: 8个数据位, 1个停止位 */
    UCSR0C = 3 << UCSZ00;
    /* 使能: 接收结束中断, 接收, 发送 */
    UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);
}

/* 接收结束中断处理函数 */
#pragma vector = USART0_RXC_vect
__interrupt void uart_rxc_isr_orig(void)
{
    //uart_rxc_isr();
}

/* 数据寄存器空中断处理函数 */
#pragma vector = USART0_UDRE_vect
__interrupt void uart_udre_isr_orig(void)
{
    //uart_udre_isr();
}

/* 读1字节, 无数据则忙等 */
u8 uart_getchar(void)
{
    if (TESTBIT(UCSR0B, RXCIE0))
        CLEARBIT(UCSR0B, RXCIE0);
    while (!(UCSR0A & (1 << RXC0)))
        _WDR();
    return UDR0;
}

/* 尝试读1字节, 无数据返回0, 可用于检测按键 */
u8 uart_getkey(void)
{
    if (UCSR0A & (1 << RXC0))
        return UDR0;
    else
        return 0;
}

/* 接收字符串, 并回显, 需在参数中给定字符串存放位置 */
void uart_getstring(u8 *str)
{
    u8 c;
    u8 *s = str;

    while ((c = uart_getchar()) != '\r') {
        if (c == '\b') {
            if ((str - s) > 0) {
                uart_putstring("\b \b");
                str--;
            }
        } else {
            *str = c;
            str++;
            uart_putchar(c);
        }
    }
    *str = '\0';
    uart_putchar('\n');
}

/* 接收整型数, 支持8进制/10进制/16进制输入 */
u16 uart_getnum(void)
{
    u8 str[10];

    uart_getstring(str);
    return (u16)strtol((char *)str, NULL, 0);
}

/* 写1字节, 发送缓冲器满则忙等 */
void uart_putchar(u8 data)
{
      while (!(UCSR0A & (1 << UDRE0)))
        ;
    UDR0 = data;
    if (data == '\n')
        uart_putchar('\r');
}

/* 字符串输出 */
void uart_putstring(u8 *str)
{
#ifdef DEBUG
    while (*str)
        uart_putchar(*str++);
#endif
}

/* 格式化输出, 不支持浮点数 */
void uart_printf(const u8 *fmt, ...)
{
    u8 str[32];
    va_list args;

    va_start(args, fmt);
    vsprintf((char *)str, (char *)fmt, args);
    uart_putstring(str);
    va_end(args);
}
