/* ******************************************************
 * File Name: main.c
 * Function:  AVR UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能, 方便程序调试
 * Authon & Date: Joshua Chan, 2012/04/01, 2012/04/14更新
 * ******************************************************/
#include <intrinsics.h>
#include <ina90.h>
#include <pgmspace.h>
#include <comp_A90.h>
#include <string.h>
#include "uart.h"
#include "step_motor.h"
#include "hardware.h"

/* 测试程序 */
void main(void)
{
    u16 n;
    u8 s[20];

    uart_init(9600);
    _SEI();
    while (1) {
        uart_putstring("Please input a number: ");
        n = uart_getnum();
        uart_putstring("Please input a string: ");
        uart_getstring(s);
        uart_printf("number: %d, string: \"%s\"\n", n, s);
        _WDR();
    }
}
