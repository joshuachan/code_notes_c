/* *************************************************
 * File Name: timer0_set.c
 * Function:  AVR定时器0配置模块
 * Description: 指定期望的定时器中断频率，及中断处理函数，
                模块可自动完成配置
 * Authon & Date: Joshua Chan, 2012/04/01
 * *************************************************/
#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "constant.h"
#include "timer0_set.h"
#include "uart_io.h"

/* 寄存器配置与预分频值对应表 */
__flash static int prescaling[][2] = {
    {0, 0},
    {1, 1},
    {2, 8},
    {3, 32},
    {4, 64},
    {5, 128},
    {6, 256},
    {7, 1024},
};

/* 定义timer0比较匹配中断处理函数 */
#pragma vector = TIMER0_COMP_vect
__interrupt void timer0_isr_orig(void)
{
	/* 中断处理函数由外部传入 */
    timer0_isr();
}

/* 根据初步计算值选择预分频值 */
static unsigned char prescale_index(int num)
{
    unsigned char i;

    for (i = 0; i < 8; i++)
        if (num <= prescaling[i][1])
            return i;
    return 0;
}

/* 配置8位定时器timer0比较匹配中断使能
 * @freq: 定时器中断发生频率
 */
void timer0_comp_init(int freq)
{
    unsigned char index;
    int prescale_tmp;
    int ocr0;

    prescale_tmp = (FOSC / freq / COUNT_MAX); /* 初步计算预分频值 */
    index = prescale_index(prescale_tmp);	  /* 根据上一步结果选择预分频值 */
    ocr0 = ((FOSC / prescaling[index][1] / freq) - 1); /* 计算比较值 */

	/* 根据上面的运算结果配置寄存器 */
    TCCR0 = (1<<WGM01) | (prescaling[index][0]<<CS00);
    OCR0 = (unsigned char)ocr0;
    TIMSK |= (1 << OCIE0);
    TCNT0 = 0;
}

void timer0_enable(void)
{
    TIMSK |= (1 << OCIE0);
}

void timer0_disable(void)
{
    TIMSK &= ~(1 << OCIE0);
}
