/* ************************************************
 * File name: main.c
 * Function:  AVR的UART数据收发测试程序
 * Description: 实现将RXD端口收到的数据转发至TXD端口
 *              收发工作由UART中断自动完成
 * Author & Date: Joshua Chan, 2012/03/28
 * ************************************************/
#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "uart_test.h"

/* 测试程序: 将串口接收的数据原样送出 */
void main(void)
{
    unsigned char data;

    fifo_pos_init();            /* 重置位置记录 */
    uart_init(B9600);           /* 初始化UART */
	_SEI();
    while (1) {
        if ((data = rx_byte()) != NONE_DATA)
            tx_byte(data);
        _WDR();                 /* 喂狗 */
    }
}
