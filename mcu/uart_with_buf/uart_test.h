/* ************************************************
 * File name: uart_test.h
 * Function:  AVR的UART数据收发测试程序
 * Description: 实现将RXD端口收到的数据转发至TXD端口
 *              收发工作由UART中断自动完成
 * Author & Date: Joshua Chan, 2012/03/28
 * ************************************************/
#ifndef _UART_TEST_H
#define _UART_TEST_H

/* 定义FOSC=16.0MHz时的UBRR值 */
__flash enum baudrate {
    B2400 = 416,
    B4800 = 207,
    B9600 = 103,
    B14400 = 68,
    B19200 = 51,
    B28800 = 34,
    B38400 = 25,
    B57600 = 16,
    B76800 = 12,
    B115200 = 8,
    B230400 = 3,
};

#define NONE_DATA  '\4'        /* 无新数据标志 */
#define RX_BUF_SIZE 32         /* 接收缓冲区大小 */
#define TX_BUF_SIZE 32         /* 发送缓冲区大小 */

/* 重置位置记录 */
extern void fifo_pos_init(void);

/* UART初始化 */
extern void uart_init(unsigned int baud);

/* 从接收缓冲区中读1字节 */
extern unsigned char rx_byte(void);

/* 将1字节数据送入发送缓冲区, 同时使能数据寄存器空中断 */
extern void tx_byte(unsigned char data);

#endif
