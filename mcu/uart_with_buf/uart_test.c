/* ************************************************
 * File name: uart_test.c
 * Function:  AVR的UART数据收发测试程序
 * Description: 实现将RXD端口收到的数据转发至TXD端口
 *              收发工作由UART中断自动完成
 * Author & Date: Joshua Chan, 2012/03/28
 * ************************************************/
#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "uart_test.h"

unsigned char rx_fifo[RX_BUF_SIZE]; /* 接收缓冲区 */
unsigned char tx_fifo[TX_BUF_SIZE]; /* 发送缓冲区 */
unsigned char rx_pos;      /* 缓冲区中最后接收数据的位置 */
unsigned char read_pos;    /* 缓冲区中最后读出数据的位置 */
unsigned char tx_pos;      /* 缓冲区中最后发送数据的位置 */
unsigned char write_pos;   /* 缓冲区中最后写入数据的位置 */

/* UART初始化 */
void uart_init(unsigned int baud)
{
    /* 先禁用USART功能 */
    UCSR0B = 0x00;
    /* 设置baudrate */
    UBRR0H = (unsigned char)(baud>>8);
    UBRR0L = (unsigned char)baud;
    /* 设置帧格式: 8个数据位, 1个停止位 */
    UCSR0C = 3 << UCSZ00;
    /* 使能: 接收结束中断, 接收, 发送 */
    UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);
}

/* 重置位置记录 */
void fifo_pos_init(void)
{
    rx_pos = 0;
    read_pos = 0;
    tx_pos = 0;
    write_pos = 0;
}

/* 接收结束中断处理函数 */
#pragma vector = USART0_RXC_vect
__interrupt void uart_rxc_isr(void)
{
    unsigned char udr0;

    /* 读取数据寄存器 */
    udr0 = UDR0;
    /* 缓冲区满则从头开始写 */
    if (++rx_pos == RX_BUF_SIZE)
        rx_pos = 0;
    /* 将读取的数据写入缓冲区 */
    rx_fifo[rx_pos] = udr0;
}

/* 从接收缓冲区中读1字节 */
unsigned char rx_byte(void)
{
    /* 判断是否有数据可读 */
    if (read_pos != rx_pos) {
        /* 缓冲区满则从头开始读 */
        if (++read_pos == RX_BUF_SIZE)
            read_pos = 0;
        return rx_fifo[read_pos];
    } else {
        /* 无数据可读返回一个标志 */
        return NONE_DATA;
    }
}

/* 数据寄存器空中断处理函数 */
#pragma vector = USART0_UDRE_vect
__interrupt void uart_udre_isr(void)
{
    /* 判断是否有数据可发 */
    if (tx_pos != write_pos) {
        if (++tx_pos == TX_BUF_SIZE)
            tx_pos = 0;
        /* 将缓冲区数据写入数据寄存器 */
        UDR0 = tx_fifo[tx_pos];
    } else {
        /* 无数据可发则禁止数据寄存器空中断, 否则此中断会一直发生 */
        UCSR0B &= ~(1 << UDRIE0);
    }
}

/* 将1字节数据送入发送缓冲区, 同时使能数据寄存器空中断 */
void tx_byte(unsigned char data)
{
    if (++write_pos == TX_BUF_SIZE)
        write_pos = 0;
    /* 数据送入缓冲区 */
    tx_fifo[write_pos] = data;
    /* 使能中断, 以便自动发送 */
    UCSR0B |= (1 << UDRIE0);
}
