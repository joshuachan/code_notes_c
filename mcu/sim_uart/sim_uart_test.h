/* ****************************************************************
 * File name: sim_uart_test.h
 * Function:  AVR的I/O端口模拟串口测试程序
 * Description: 1. 通过定时器中断轮流处理端口收发, 模拟UART数据收发功能
 *              2. 外部晶振频率16.0MHz, 预分频值8, 定时器timer频率9600Hz,
 *                 定时器比较值207, 模拟串口波特率4800bps, 全双工异步模式,
 *                 支持模拟1个串口, 提高定时器频率或降低波特率, 1个定时器
                   可以支持模拟更多串口
 * Author & Date: Joshua Chan, 2012/03/30
 * ****************************************************************/
#ifndef _SIM_UART_TEST_H
#define _SIM_UART_TEST_H

/* PE0: 模拟UART接收端口
 * PE1: 模拟UART发送端口
 * PE0/PE1为硬件自带UART0的RX/TX口, 测试时需将其UART功能关闭
 */
#define GET_RX() (PINE & (1<<PINE0))     /* 接收电平值 */
#define SET_TX() (PORTE |= (1<<PORTE1))  /* 发送高电平 */
#define CLR_TX() (PORTE &= ~(1<<PORTE1)) /* 发送低电平 */

#define NONE_DATA '\4'          /* 无新数据标志 */

/* 定义接收队列 */
#define RX_FIFO_SIZE 4         /* 接收队列大小 */
#define TX_FIFO_SIZE 4         /* 发送队列大小 */

/* 定时器中断发生时的读写周期 */
enum timer_turn {
    RX_TURN = 0,                /* 读周期 */
    TX_TURN,                    /* 写周期 */
};

/* 数据帧结构 */
__flash enum frame_bit {
    BIT_0 = 0,
    BIT_1,
    BIT_2,
    BIT_3,
    BIT_4,
    BIT_5,
    BIT_6,
    BIT_7,
    BIT_STOP,
    BIT_IDLE,
    BIT_START,
};

/* 变量初始化 */
extern void var_init(void);

/* 配置收发端口 */
extern void port_init(void);

/* 配置定时器 */
extern void timer0_init(void);

/* 从接收队列读1字节 */
extern unsigned char rx_byte(void);

/* 向发送队列写1字节 */
extern void tx_byte(unsigned char data);

#endif
