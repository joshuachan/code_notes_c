/* ****************************************************************
 * File name: main.c
 * Function:  AVR的I/O端口模拟串口测试程序
 * Description: 1. 通过定时器中断轮流处理端口收发, 模拟UART数据收发功能
 *              2. 外部晶振频率16.0MHz, 预分频值8, 定时器timer频率9600Hz,
 *                 定时器比较值207, 模拟串口波特率4800bps, 全双工异步模式,
 *                 支持模拟1个串口, 提高定时器频率或降低波特率, 1个定时器
                   可以支持模拟更多串口
 * Author & Date: Joshua Chan, 2012/03/30
 * ****************************************************************/
#include <ioavr.h>
#include <ina90.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <pgmspace.h>
#include "sim_uart_test.h"

/* 测试程序: 将接收的数据送出 */
void main(void)
{
    unsigned char data;

    var_init();                 /* 初始化变量 */
    port_init();                /* 配置收发端口 */
    timer0_init();              /* 配置定时器 */
    _SEI();                     /* 全局中断使能 */
    while (1) {
        if ((data = rx_byte()) != NONE_DATA)
            tx_byte(data);
        _WDR();
    }
}
