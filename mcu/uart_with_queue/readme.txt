AVR UART I/O 功能模块（带queue）

本程序实现AVR设备UART端口<输入/回显/格式化输出>功能，由此可以通过PC机终端软件与设备直接交互。

程序有参考Samsung S3C2440库文件。

程序组成：测试程序<main.c>，功能模块<uart.c>及其头文件<uart.h>。