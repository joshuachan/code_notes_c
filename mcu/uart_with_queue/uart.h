/* ****************************************************************
 * File Name: uart.h
 * Function:  AVR UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能, 便于通过终端软件人机交互
 * Authon & Date: Joshua Chan, 2012/04/01
 * ****************************************************************/

#ifndef _UART_H
#define _UART_H

#include "macro.h"
#include "queue.h"

#define RX_BUF_SIZE 128         /* 接收缓冲区大小 */
#define TX_BUF_SIZE 128         /* 发送缓冲区大小 */

extern queue_st rx_queue;

/* UART初始化 */
void uart_init(u32 baud);

/* 读1字节, 无数据则忙等 */
u8 uart_getchar(void);

/* 尝试读1字节, 无数据返回0, 可用于检测按键 */
u8 uart_getkey(void);

/* 接收字符串, 并回显, 需在参数中给定字符串存放位置 */
void uart_getstring(u8 *str);

/* 接收整型数, 支持8进制/10进制/16进制输入 */
u16 uart_getnum(void);

/* 写1字节, 发送缓冲器满则忙等 */
void uart_putchar(u8 data);

/* 字符串输出 */
void uart_putstring(u8 *str);

/* 格式化输出, 字符串不可太长 */
void uart_printf(const u8 *fmt, ...);

#ifndef DEBUG
#define DBG(arg) (0)
#define DBGF(...) (0)
#else
#define DBG(arg) uart_putstring(arg)
#define DBGF(fmt, ...) uart_printf(fmt, ##__VA_ARGS__)
#endif

#endif
